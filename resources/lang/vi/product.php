<?php

return [
    'review_success' => 'Phản hồi của bạn đã được gửi đi.',
    'review_error' => 'Đã xảy ra lỗi! Vui lòng thử lại.'
];