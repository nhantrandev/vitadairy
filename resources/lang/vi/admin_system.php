<?php
return [
    'title' => 'System',

    'list' => 'Config system',

    'system' => 'system',

    'form' => [
        'contact_email' => 'Email system (The email will receive contact)',
        'email' => 'Email',
        'location' => 'Location',
        'phone' => 'Phone top',
        'phone_bottom' => 'Phone bottom',
        'address' => 'Address',
        'fax' => 'Fax',
        'google_analytic' => 'Google analytic',
        'chat_script' => 'Chat script',

        'facebook' => 'Facebook',
        'youtube' => 'Youtube',
        'google' => 'Google plus',
        'twitter' => 'Twitter',
        'likedin' => 'Liked In',
        'instagram' => 'Instagram',
        'tel_phone' => 'Whatsapp',

        'website_title' => 'Website title',
        'website_description' => 'Website description',
        'website_keywords' => 'Website keywords',

        'line' => 'Line',
        'viber' => 'Viber',
        'phone_1' => 'Phone 1',
        'phone_2' => 'Phone 2',
        'phone_3' => 'Phone 3',

        'logo' => 'Logo',
        'privacy_security' => 'Privacy security',
        'about'=>'About'
    ],
    'website_info' => 'Website info',
    'social' => 'Social'
];