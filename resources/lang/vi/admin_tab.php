<?php
return [
    'general' => 'General',
    'information' => 'Information',
    'photos' => 'Photos',
    'blocks' => 'Blocks',
    'conversation' => 'Conversation',
    'card' => 'Card Information',
    'feature' => 'Feature',
    'link' => 'Link',
    'price' => 'Price',
    'media' => 'Media',
    'capacity' => 'Capacity'
];