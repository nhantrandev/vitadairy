<?php 


return [
    'error_login' => 'This account is not found',
    'false_all' => 'There are something wrong',
    'is_expired' => 'Your :attr is expired',
    'verify_phone' => "Your code is :token",
    'not_exist' => 'The :attribute is exist',
    'error_update_profile' => 'You can not change your profile',
    'success_update_profile' => 'Your profile have changed',
    'inactive' => 'Your account have not ever in-active',
    'prohibit' => 'Your account have not ever prohibit',
    'wrong_password' => 'Your credential is incorrect',
    'exist' => 'Your :attr is taken',
    'wrong_reset' => 'The token or email is wrong !'
];