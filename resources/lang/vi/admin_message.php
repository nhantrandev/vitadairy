<?php
return [
    'alert_delete' => 'Are you sure to delete <strong>:attr</strong>?',
    'created_successful' => 'The :attr has been created successfully',
    'updated_successful' => 'The :attr has been updated successfully',
    'close_successful' => 'The :attr has been updated successfully',
    'deleted_successful' => 'The :attr has been deleted successfully',
    'send_sms_successful' => 'The message is send successfully',
    'no_data' => "Don't have any data",


    'created_error' => 'The :attr has been created fail',
    'updated_error' => 'The :attr has been updated fail',
    'deleted_error' => 'The :attr has been deleted fail',
    'send_sms_error' => 'The message is send fail because of :message',

    'not_permission' => 'You dont have permission',

    'least_one' => "Please select at least one :attribute",
];