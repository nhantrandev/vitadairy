<?php

return [
    'name' => 'Vui lòng nhập tên',
    'email' => 'Vui lòng nhập một địa chỉ email hợp lệ',
    'phone' => 'Vui lòng nhập số điện thoại',
    'password_required' => 'Vui lòng nhập mật khẩu',
    'password_minlength' => 'Mật khẩu phải ít nhất 6 ký tự',
    'password_equalTo' => 'Vui lòng nhập cùng mật khẩu như trên',
    'shipname' => 'Vui lòng nhập tên người nhận hàng',
    'shipphone' => 'Vui lòng nhập số điện thoại',
    'shipaddress' => 'Vui lòng nhập địa chỉ giao hàng',
    'shipdistrict' => 'Vui lòng nhập Quận/Huyện giao hàng',
    'shipcity' => 'Vui lòng nhập Tỉnh/Thành phố giao hàng'
];