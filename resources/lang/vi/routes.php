<?php
return [
    'login' => 'login',
    'registry' => 'registry',
    'forgot' => 'forgot',
    'reset' => 'reset',
    'logout' => 'logout',
    'profile' => 'profile',
    'profile_edit' => 'profile/edit',

    'product_category' => 'danh-muc/{category_slug}',
    'product_detail' => 'danh-muc/{category_slug}/{product_slug}',
];