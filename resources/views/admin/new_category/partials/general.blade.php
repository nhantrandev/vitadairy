<div class="row">
    <div class="col-md-4">
        <div class="font-bold col-pink">{!! trans("admin_new.form.image") !!}</div>
        <div class="form-group">
            @component('admin.layouts.components.upload_photo', [
                'image' => $new->image ?? null,
                'name' => 'image',
            ])
            @endcomponent
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-pink">{!! trans("admin_new.form.active") !!}</div>
        <div class="form-group">
            <input type="checkbox" id="active" name="active" value="1" {!! isset($new) && $new->active ?
            "checked" : null !!}>
            <label for="active">{!! trans("admin_new.form.active") !!}</label>
        </div>
    </div>
</div>