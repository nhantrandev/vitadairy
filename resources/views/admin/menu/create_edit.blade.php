@extends("admin.layouts.master")

@section("content")
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">

                    @include('admin.layouts.partials.message')

                    @component('admin.layouts.components.form', [
                        'form_method' =>  empty($menu) ? 'POST' : 'PUT',
                        'form_url' => empty($menu) ? route("admin.menu.store") : route("admin.menu.update", $menu->id)
                    ])
                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Vị trí</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="position" value="{{ isset($menu) ? $menu->position : old('position') }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Nav tabs -->
                        @include('admin.translation.nav_tab', [
                            'default_tabs' => [],
                            'object_trans' => $menu ?? null,
                            'default_tab' => $composer_locale,
                            'form_fields' => [
                                ['type' => 'text', 'name' => 'name'],
                                ['type' => 'text', 'name' => 'url']
                            ],
                            'translation_file' => 'admin_menus'
                        ])

                        {{--Buttons--}}
                        @include("admin.layouts.partials.form_buttons", [
                            "cancel" => route("admin.menu.index")
                        ])
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

    @if($composer_locale !== 'en')
        <script type="text/javascript" src="/assets/plugins/jquery-validation/localization/messages_{{ $composer_locale }}.js"></script>
    @endif
    <script type="text/javascript" src="/assets/admin/js/pages/menu.create.js"></script>
@endsection