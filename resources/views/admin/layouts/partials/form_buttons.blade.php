<div class="clearfix"></div>
<hr>
<div class="row">
    @if(!isset($is_show))
    <div class="col-sm-9">
        <button type="submit" class="btn btn-primary waves-effect btn_submit">{!! trans("button.save") !!}</button>
        <a href="" class="btn btn-default waves-effect">{!! trans("button.reset") !!}</a>
    </div>
    @endif
    <div class="col-sm-{{ !isset($is_show) ? '3' : '12' }} text-right">
        <a href="{!! $cancel !!}" class="btn btn-danger waves-effect">{!! trans("button.cancel") !!}</a>
    </div>
</div>