<div class="row">
    <div class="col-md-4">
        <div class="font-bold col-pink">Hình ảnh</div>
        <div class="form-group">
            @component('admin.layouts.components.upload_photo', [
                'image' => $new->image ?? null,
                'name' => 'image',
            ])
            @endcomponent
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-pink">Danh mục</div>
        <div class="form-group form-float">
            <div class="form-line">
                <select name="new_category_id" id="new_category_id" class="form-control">
                    <option value="">---</option>
                    @if(isset($new_category))
                    @foreach($new_category as $category)
                    <option value="{{ $category->id }}" {{ isset($new) && $new->new_category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="font-bold col-pink">Active ?</div>
        <div class="form-group">
            <input type="checkbox" id="active" name="active" value="1" {!! isset($new) && $new->active ?
            "checked" : null !!}>
            <label for="active">Active</label>
        </div>
    </div>
</div>