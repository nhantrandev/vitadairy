@extends("admin.layouts.master")

@section("content")
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {!! trans("admin_system.list") !!}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="body">

                    @include("admin.layouts.partials.message")

                    <form id="form-form" method="post"
                          action="{!! route("admin.system.update", '0110') !!}"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Logo</div>
                                <div class="form-group">
                                @component('admin.layouts.components.upload_photo', [
                                    'image' => $system['website_logo']['content'] ?? null,
                                    'name' => 'website_logo',
                                ])
                                @endcomponent
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                <div class="font-bold col-pink">{!! trans('admin_system.form.contact_email') !!}</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="email" name="contact_email" class="form-control"
                                               value="{{ $system['contact_email']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div> --}}
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Tiêu đề Website</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="website_title" class="form-control"
                                               value="{{ $system['website_title']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Tên Domain</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="website_domain" class="form-control"
                                               value="{{ $system['website_domain']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Email</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="email" class="form-control"
                                               value="{{ $system['email']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Hotline</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="hotline" class="form-control"
                                               value="{{ $system['hotline']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3>Địa chỉ Footer</h3>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Địa chỉ văn phòng tại Hà Nội</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="address_ha_noi" class="form-control"
                                                value="{{ $system['address_ha_noi']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Tel/Fax văn phòng tại Hà Nội</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="tel_ha_noi" class="form-control"
                                                value="{{ $system['tel_ha_noi']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Địa chỉ văn phòng tại Tp. Hồ Chí Minh</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="address_tphcm" class="form-control"
                                                value="{{ $system['address_tphcm']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="font-bold col-pink">Tel/Fax văn phòng tại Tp. Hồ Chí Minh</div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="tel_tphcm" class="form-control"
                                                value="{{ $system['tel_tphcm']['content'] ?? null }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--Buttons--}}
                        @include("admin.layouts.partials.form_buttons", [
                            "cancel" => ''
                        ])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
