@extends("admin.layouts.master")

@section("style")
    <link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/assets/plugins/select2/css/select2.min.css"/>
@endsection

@section("content")
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">

                    @include('admin.layouts.partials.message')

                    @component('admin.layouts.components.form', [
                        'form_method' =>  empty($product_category) ? 'POST' : 'PUT',
                        'form_url' => empty($product_category) ? route("admin.product.category.store") : route("admin.product.category.update", $product_category->id)
                    ])
                        <div class="col-md-4" style="display: none">
                            <div class="font-bold col-pink">{!! trans("admin_product.form.parent") !!}</div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select name="parent_id" id="parent_id" class="form-control">
                                        <option value="">---</option>
                                        @if(isset($parents))
                                        @foreach($parents as $category)
                                        <option value="{{ $category->id }}" {{ isset($product_category) && $product_category->parent_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    
                        @include('admin.translation.nav_tab', [
                            'default_tab' => $composer_locale,
                            'object_trans' => $product_category ?? null,
                            'form_fields' => [
                                ['type' => 'text', 'name' => 'name']
                            ],
                            'translation_file' => 'admin_product_category'
                        ])

                        {{--Buttons--}}
                        @include("admin.layouts.partials.form_buttons", [
                            "cancel" => route("admin.product.category.index")
                        ])
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/plugins/jquery-ui-sortable/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

    @if($composer_locale !== 'en')
        <script type="text/javascript" src="/assets/plugins/jquery-validation/localization/messages_{{ $composer_locale }}.js"></script>
    @endif
    <script type="text/javascript" src="/assets/admin/js/pages/product_category.create.js"></script>
@endsection