<div class="row">
    <div class="col-md-4">
        <div class="font-bold col-pink">{!! trans("admin_product.form.image") !!}</div>
        <div class="form-group">
            @component('admin.layouts.components.upload_photo', [
                'image' => $product->image ?? null,
                'name' => 'image',
            ])
            @endcomponent
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-pink">{!! trans("admin_product.form.active") !!}</div>
        <div class="form-group">
            <input type="checkbox" id="active" name="active" value="1" {!! isset($product) && $product->active ?
            "checked" : null !!}>
            <label for="active">{!! trans("admin_product.form.active") !!}</label>
        </div>
    </div>
</div>