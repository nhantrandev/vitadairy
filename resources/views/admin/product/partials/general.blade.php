<div class="row">
    <div class="col-md-4">
        <div class="font-bold col-pink">Hình ảnh</div>
        <div class="form-group">
            @component('admin.layouts.components.upload_photo', [
                'image' => $product->image ?? null,
                'name' => 'image',
            ])
            @endcomponent
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-pink">Danh mục</div>
        <div class="form-group form-float">
            <div class="form-line">
                <select name="product_category_id" id="product_category_id" class="form-control">
                    <option value="">---</option>
                    @if(isset($product_category))
                    @foreach($product_category as $category)
                    <option value="{{ $category->id }}" {{ isset($product) && $product->product_category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="font-bold col-pink">Active ?</div>
        <div class="form-group">
            <input type="checkbox" id="active" name="active" value="1" {!! isset($product) && $product->active ?
            "checked" : null !!}>
            <label for="active">Active</label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-pink">Is Popup ?</div>
        <div class="form-group">
            <input type="checkbox" id="is_popup" name="is_popup" value="1" {!! isset($product) && $product->is_popup ? "checked" : null !!}>
            <label for="is_popup">Is Popup</label>
        </div>
    </div>
</div>