@if($paginator->hasPages())
<ul class="pagination">
    {{-- Previous Page Link --}}
    @if(!$paginator->onFirstPage())
    <li class="page-item disabled">
        <a class="page-link" href="{{ $paginator->previousPageUrl() }}"><span class="arrow_left"></span>
        </a>
    </li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
        {{-- "Three Dots" Separator --}}
        @if (is_string($element))
        <span class='page-numbers'>{{ $element }}</span>
        <li class="page-item">
            <span class="page-link">{{ $element }}
            </span>
        </li>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                <li class="page-item active">
                    <a class="page-link" href="#">{{ $page }}
                    </a>
                </li>
                @else
                <li class="page-item">
                    <a class="page-link" href="{{ $url }}">{{ $page }}
                    </a>
                </li>
                @endif
            @endforeach
        @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li class="page-item">
        <a class="page-link" href="{{ $paginator->nextPageUrl() }}"><span class="arrow_right"></span>
        </a>
    </li>
    @endif
</ul>
@endif