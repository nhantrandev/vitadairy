@extends('frontend.master')

@section('content')

<!-- 404-->
<section class="sPages sUser s404">
    <div class="container">
        <div class="s404__img"><img src="/images/404.svg" alt=""></div>
        <h1 class="s404__title">Trang bạn truy cập <span>không tồn tại</span></h1>
        <div class="s404__desc">Vui lòng kiểm tra lại hoặc trở về trang chủ</div>
        <div class="s404__link"><a class="btn btn-primary" href="/" title="Trở về trang chủ"> <i class="icon_house"></i>Trở
                về trang chủ</a></div>
    </div>
</section>
<!-- End 404-->

@endsection