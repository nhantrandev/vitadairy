@extends('frontend.master')

@section('content')
<section class="mainChild">  
        <div class="container">
          <div class="mainChildContent">
            <div class="text-center">
              <h1 class="heading-4">Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</h1>
            </div>
            <div class="documnet">
              <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
              <p><img src="/images/img1.jpg" alt=""></p>
              <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
            </div>
          </div>
          <div class="heading-2 heading-other">Bài viết liên quan</div>
          <div class="sNewsSlide girdNews newsRelative">
            <div class="sNews__inner__item">
              <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
              </div>
              <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                </div>
                <div class="btnLink"><a href="#">Xem chi tiết </a></div>
              </div>
            </div>
            <div class="sNews__inner__item">
              <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
              </div>
              <div class="info"><a href="#"> Không cần  kháng sinh, mẹ vẫn tăng cường sức khỏe con nhờ kháng thể này</a>
                <div class="des">Tận dụng hiệu quả nguồn kháng thể IgG tự nhiên có trong sữa non là mẹ đã giúp con tăng cường sức đề kháng, “nghỉ chơi” với kháng sinh.
                </div>
                <div class="btnLink"><a href="#">Xem chi tiết </a></div>
              </div>
            </div>
            <div class="sNews__inner__item">
              <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
              </div>
              <div class="info"><a href="#"> Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</a>
                <div class="des">IgG là yếu tố quan trọng cho hệ miễn dịch của bé. IgG phản ứng lại với những “khách lạ” ghé thăm cơ thể chúng ta như vi khuẩn gây bệnh hoặc truyền bệnh
                </div>
                <div class="btnLink"><a href="#">Xem chi tiết </a></div>
              </div>
            </div>
            <div class="sNews__inner__item">
              <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
              </div>
              <div class="info"><a href="#"> Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</a>
                <div class="des">IgG là yếu tố quan trọng cho hệ miễn dịch của bé. IgG phản ứng lại với những “khách lạ” ghé thăm cơ thể chúng ta như vi khuẩn gây bệnh hoặc truyền bệnh
                </div>
                <div class="btnLink"><a href="#">Xem chi tiết </a></div>
              </div>
            </div>
          </div>
        </div>
      </section>

@endsection

@section('script')

@endsection