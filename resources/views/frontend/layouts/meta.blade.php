@if(isset($metadata))
    <title>{{ $metadata->title }} | {{ getAppName() }}</title>
    
    <meta name="description" content="{{ $metadata->description }}"/>
    <meta name="keywords" content="{{ $metadata->key_word }}"/>
    <link rel="canonical" href="{!! Request::fullUrl() !!}" /> 

    <meta property="og:title" content="{{ $metadata->title }}"/>
    <meta property="og:description" content="{{ $metadata->description }}"/>
    @if($metadata->image_seo)
        <meta property="og:image" content="{!! getAppUrl().$metadata->image_seo !!}"/>
    @endif
    <meta property="og:url" content="{!! Request::fullUrl() !!}"/>
    <meta property="og:site_name" content="{{ getAppName() }}"/>
    <meta property="og:type" content="website"/>

    <meta name="twitter:title" content="{{ $metadata->title }}"/>
    <meta name="twitter:description" content="{{ $metadata->description }}"/>
    @if($metadata->image_seo)
        <meta property="twitter:image" content="{!! getAppUrl().$metadata->image_seo !!}"/>
    @endif
    <meta name="twitter:card" content="summary_large_image"/>
    <meta property="fb:app_id" content="{!! getFacebookClientId() !!}"/>
    <meta name="twitter:site" content="{{ getAppName() }}"/>
@endif