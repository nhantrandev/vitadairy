<footer class="footer">
    <div class="container">
        <div class="logoFooter"><img src="{{ System::content('website_logo', '/images/logo.svg') }}"></div>
        <address>
            <div class="row">
                <div class="col-md-4">
                    <h4>{{ System::content('website_title', 'CÔNG TY CỔ PHẦN SỮA VITADAIRY VIỆT NAM') }}</h4>
                    <p>Website: <a href="{{ System::content('website_domain', 'www.vitadairy.vn') }}">{{ System::content('website_domain', 'www.vitadairy.vn') }}</a></p>
                    <p>Email: <a href="mailto:{{ System::content('email', 'info@vitadairy.vn') }}">{{ System::content('email', 'info@vitadairy.vn') }}</a></p>
                    <p>Hotline: <a href="tel+ {{ System::content('hotline', '1900 636 958') }}">{{ System::content('hotline', '1900 636 958') }}</a></p>
                </div>
                <div class="col-md-4">
                    <h4>Văn phòng tại Hà Nội</h4>
                    <p>{{ System::content('address_ha_noi', 'Tầng 20, Khách sạn Mường Thành Grand Hà Nội, Lô CC2 Bắc Linh Đàm, Q. Hoàng Mai') }}</p>
                    <p>Tel/ Fax: {{ System::content('tel_ha_noi', '(024) 3540 9193') }}</p>
                </div>
                <div class="col-md-4">
                    <h4>Trụ sở tại TP. Hồ Chí Minh</h4>
                    <p>{{ System::content('address_tphcm', 'Tầng 09, Empire Tower 26-28, Hàm Nghi, P. Bến Nghé, Q.1') }}</p>
                    <p>Tel/ Fax: {{ System::content('tel_tphcm', '(0286) 6509 3333') }}</p>
                </div>
            </div>
        </address>
    </div>
    <div class="share">
        <a href="{{ System::content('youtube') }}" target="_blank">
            <img src="/images/youtube.png" alt="">
        </a>
        <a href="{{ System::content('facebook') }}" target="_blank">
            <img src="/images/facebook.png" alt="">
        </a>
    </div>
    <a class="onTop" href="#"></a>
</footer>