@if(session()->has("success"))
    <div class="alert alert-success alert-dismissible message-errors" role="alert">
        {{ session("success") }}
    </div>
@endif

@if(session()->has("error"))
    <div class="alert alert-danger alert-dismissible message-errors" role="alert">
        {{ session("error") }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger message-errors">
        <ul style="padding-left: 0; list-style: none;">
            @foreach ($errors->toArray() as $key => $error)
                @if($key != 'is_modal')
                    @foreach($error as $err)
                        <li>{{ $err }}</li>
                    @endforeach
                @endif
            @endforeach
        </ul>
    </div>
@endif
