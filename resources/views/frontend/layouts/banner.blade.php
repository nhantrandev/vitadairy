<!-- Banner-->
<section class="sBanner">
    <div class="banner">
        <div class="banner__inner">
            <div class="banner__item" style="background-image: url(/images/banner/slide-1.png)"><img src="/images/banner/slide-1.png"
                    alt="">
                <div class="container">
                    <div class="banner__item__inner">
                        <div class="banner__item__content">
                            <h3 class="banner__item__sub">Combo tháng tư</h3>
                            <h2 class="banner__item__title"><span class="line-1">Huyết thanh &</span><span class="line-2">Dịch
                                    vụ làm đẹp</span></h2>
                            <div class="banner__item__desc">Bạn sẽ được tận hưởng các dịch vụ làm đẹp chuyên nghiệp và
                                hiệu quả nhất từ các chuyên gia của H&H</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End slide-->
            <div class="banner__item" style="background-image: url(/images/banner/slide-2.jpg)"><img src="/images/banner/slide-2.jpg"
                    alt="">
                <div class="container">
                    <div class="banner__item__inner">
                        <div class="banner__item__content">
                            <h3 class="banner__item__sub">Combo tháng tư</h3>
                            <h2 class="banner__item__title"><span class="line-1">Huyết thanh &</span><span class="line-2">Dịch
                                    vụ làm đẹp</span></h2>
                            <div class="banner__item__desc">Bạn sẽ được tận hưởng các dịch vụ làm đẹp chuyên nghiệp và
                                hiệu quả nhất từ các chuyên gia của H&H</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End slide -->
        </div>
        <!-- End Slides content-->
        <div class="container">
            <div class="banner__dots"></div>
        </div>
        <!-- End Pagination-->
    </div>
</section>
<!-- End banner-->