<header class="header">
    <div class="container"><a class="navbar-toggle" href="#"><span></span><span></span><span></span><span></span></a>
        <div class="row align-items-center">
            <div class="col-md-4">
                <div class="navbar-logo"><a class="navbar-brand" href="/"><img src="{{ System::content('website_logo', '/images/logo.svg') }}"></a></div>
            </div>
            <div class="col-md-8">
                <div class="menuMain">
                    <ul class="navbar-nav navbar-nav-right justify-content-between" id="navbar-238">
                        @if(isset($composer_menu))
                        @foreach($composer_menu as $menu)
                        <li class="nav-item"><a class="nav-link" href="{{ $menu->url }}" title="{{ $menu->name }} ">{{ $menu->name }} </a>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>