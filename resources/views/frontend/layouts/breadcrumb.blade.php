<section class="sBreadcrumb">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach($breadcrumbs as $key => $value)
                    @if($value["link"])
                        <li class="breadcrumb-item"><a href="{{ $value["link"] }}" title="{{ $value["name"] }}">{{ $value["name"] }}</a></li>
                    @else
                        <li class="breadcrumb-item active" aria-current="page">{{ summary($value["name"], 55) }}</li>
                    @endif
                @endforeach
            </ol>
        </nav>
    </div>
</section>