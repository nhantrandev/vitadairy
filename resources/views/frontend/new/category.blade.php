@extends('frontend.master')

@section('content')

<section class="mainChild">
    <div class="container">
        <div class="text-center">
            <h1 class="heading-4">{{ $category->name }} </h1>
        </div>
        <div class="girdNews sNews__inner">
            <div class="row">
                @if(isset($category->news))
                @foreach($category->news()->where('active', 1)->paginate(9) as $new)
                <div class="col-md-4 col-sm-6">
                    <div class="sNews__inner__item" data-tags="news-filter">
                        <div class="img-bg" style="background-image: url('{{ $new->image }}')">
                            <img src="{{ $new->image }}" alt="">
                        </div>
                        <div class="info">
                            <a href="{{ route('new.detail.get', ['new_category_slug' => $category->slug, 'new_slug' => $new->slug]) }}"> {{ $new->title }}</a>
                            <div class="des">{!! $new->description !!}</div>
                            <div class="btnLink">
                                <a href="{{ route('new.detail.get', ['new_category_slug' => $category->slug, 'new_slug' => $new->slug]) }}">Xem chi tiết </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
        <div class="text-center">
            {!! $category->news()->where('active', 1)->paginate(9)->links('paginations.index') !!}
        </div>
    </div>
</section>

@endsection