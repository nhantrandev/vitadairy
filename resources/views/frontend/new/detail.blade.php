@extends('frontend.master')

@section('content')

<section class="mainChild">
    <div class="container">
        <div class="mainChildContent">
            <div class="text-center">
                <h1 class="heading-4">{{ $new->title }}</h1>
            </div>
            <div class="documnet">
                <p>{!! $new->description !!}</p>
                <p><img src="{{ $new->image }}" alt=""></p>
                {!! $new->content !!}
            </div>
        </div>
        @if(isset($new_related))
        <div class="heading-2 heading-other">Bài viết liên quan</div>
        <div class="sNewsSlide girdNews newsRelative">
            @foreach($new_related as $item)
            <div class="sNews__inner__item">
                <div class="img-bg" style="background-image: url('{{ $item->image }}')">
                    <img src="{{ $item->image }}" alt="">
                </div>
                <div class="info">
                    <a href="{{ route('new.detail.get', ['new_category_slug' => $item->category->slug, 'new_slug' => $item->slug]) }}"> {{ $item->title }}</a>
                    <div class="des">{!! $item->description !!}</div>
                    <div class="btnLink">
                        <a href="{{ route('new.detail.get', ['new_category_slug' => $item->category->slug, 'new_slug' => $item->slug]) }}">Xem chi tiết </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</section>

@endsection