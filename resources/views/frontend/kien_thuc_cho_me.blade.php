@extends('frontend.master')

@section('content')

<section class="mainChild">  
        <div class="container">
          <div class="text-center">
            <h1 class="heading-4">Kiến thức cho mẹ  </h1>
          </div>
          <div class="girdNews sNews__inner">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="text-center">
            <ul class="pagination">
              <li class="page-item"><a class="page-link" href="#"><span class="arrow_left"></span></a></li>
              <li class="page-item active"><a class="page-link" href="#">01</a></li>
              <li class="page-item"><a class="page-link" href="#">02</a></li>
              <li class="page-item"><a class="page-link" href="#"><span class="arrow_right"></span></a></li>
            </ul>
          </div>
        </div>
      </section>

@endsection