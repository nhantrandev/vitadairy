@extends('frontend.master')

@section('content')

<div class="sBanner" data-waypoint="100%">
        <div class="sBanner__inner">
          <div class="sBanner__item">
            <div class="img-bg" style="background-image: url(/images/slider/banner1.jpg)"><img src="/images/slider/banner1.jpg" alt="">
            </div>
            <div class="container">
              <div class="sBanner__item__img"><img src="/images/slider/colosbaby.png" alt=""></div>
              <div class="sBanner__item__title">
                <h1>TĂNG CƯỜNG <br/> MIỄN DỊCH</h1>
                <div class="image"> <img src="/images/slider/khangthetusuanon.png" alt=""></div>
                <h3>SỨC MẠNH BÊN TRONG</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--su menh tam nhin-->
      <section class="sImmune" id="tang-cuong-he-mien-dich" data-waypoint="80%">
        <div class="container">
          <div class="heading text-center">
            <h1>NUÔI CON THÔNG MINH LÀ NUÔI KỲ VỌNG CỦA MẸ CHO CON MIỄN DỊCH KHOẺ MỚI LÀ VÌ CON !!</h1>
            <p>Mẹ có biết, hai năm đầu đời là khung thời gian quan trọng cho sự phát triển và nuôi dưỡng hệ miễn dịch của trẻ, hệ miễn dịch khỏe mạnh không chỉ có ý nghĩa trong những năm tháng đầu đời mà còn tác động lâu dài đến trẻ, có thể suốt cuộc đời. Nuôi dưỡng hệ miễn dịch khoẻ, là nuôi dưỡng cho con nền tảng bên trong hoàn thiện , giúp trẻ khỏe mạnh để học hỏi tốt hơn, từ đó phát triển tối ưu về mặt trí tuệ để sẵn sàng đón nhận nhiều cơ hội trong tương lai.</p>
          </div>
          <div class="sImmune__inner">
            <h2 class="text-center"> IgG TĂNG CƯỜNG MIỄN DỊCH <br/>CHỐNG BỆNH VẶT</h2>
          </div>
          <div class="sImmune__wrap">
            <div class="image"><img src="/images/aboutus/tang-cuong-he-mien-dich.png" alt=""></div>
            <div class="sImmune__item positionAb-1">
              <div class="icon"><img src="/images/aboutus/virut.png" alt=""></div>
              <p>IgG bám dính vào màng của Virus, vi khuẩn, đánh dấu để tế bào miễn dịch nhận diện và tiêu diệt</p>
            </div>
            <div class="sImmune__item positionAb-2">
              <div class="icon"><img src="/images/aboutus/khangthe.png" alt=""></div>
              <p>Immunoglobulin G (IgG) là loại kháng thể chiếm khoảng 75% tổng kháng thể có trong huyết thanh và dịch ngoại tế bào</p>
            </div>
            <div class="sImmune__item positionAb-3">
              <div class="icon"><img src="/images/aboutus/ditruyen.png" alt=""></div>
              <p>IgG được truyền từ mẹ sang con qua sữa mẹ, đặc biệt quan trọng trong “khoảng trống miễn dịch” từ 0-36 tháng tuổi</p>
            </div>
            <div class="sImmune__item positionAb-4">
              <div class="icon"><img src="/images/aboutus/bosung.png" alt=""></div>
              <p>IgG cần được bổ sung vào sữa đủ hàm lượng mới tạo ra khả năng miễn dịch hiệu quả</p>
            </div>
          </div>
        </div>
      </section>
      <section class="sAbout" id="bo-sung-khang-the" data-waypoint="80%">
        <div class="container">
          <div class="sAbout__inner"><img src="/images/aboutus/colosbaby.png" alt="">
            <h4>Bổ sung kháng thể IgG tự nhiên từ sữa non. Cho con sức mạnh bên trong</h4>
          </div>
          <div class="sAbout__content">
            <div class="sAbout__wrap">
              <div class="image"><img src="/images/aboutus/cows.png" alt=""></div>
              <div class="sAbout__item positionAb-5">
                <div class="icon"><img src="/images/aboutus/icon5.svg" alt=""></div>
                <div class="info">  
                  <h4>TĂNG CƯỜNG MIỄN DỊCH  SỨC ĐỀ KHÁNG HIỆU QUẢ</h4>
                  <p>Bổ sung kháng thể cao IgG từ sữa non là cách trực tiếp để cơ thể bé có lượng IgG cao ổn định. Luôn sẵn sàng bảo vệ bé khỏi các tác nhân gây bệnh, giúp bé khoẻ mạnh và ít ốm vặt</p>
                </div>
              </div>
              <div class="sAbout__item positionAb-1">
                <div class="icon"><img src="/images/aboutus/icon1.svg" alt=""></div>
                <div class="info">  
                  <p>Giảm tỷ lệ số lần nhiễm khuẩn hô hấp trên: 56%                    </p>
                </div>
              </div>
              <div class="sAbout__item positionAb-2">
                <div class="icon"><img src="/images/aboutus/icon2.svg" alt=""></div>
                <div class="info">  
                  <p>Giảm tỷ lệ số lần tiêu chảy: 39%</p>
                </div>
              </div>
              <div class="sAbout__item positionAb-3">
                <div class="icon"><img src="/images/aboutus/icon3.svg" alt=""></div>
                <div class="info">  
                  <p>Giảm tỷ lệ số lần nhiễm khuẩn khác: 42%</p>
                </div>
              </div>
              <div class="sAbout__item positionAb-4">
                <div class="icon"><img src="/images/aboutus/icon4.svg" alt=""></div>
                <div class="info">  
                  <p>Giảm số lần nhập viện do các nguyên nhân trên 59%</p>
                </div>
              </div>
            </div>
          </div>
          <div class="sAbout__info">
            <div class="row">
              <div class="col-lg-3 col-sm-6">
                <div class="sAbout__info__item">
                  <div class="icon"><img src="/images/aboutus/icon6.svg" alt=""></div>
                  <h4>NGỦ NGON MAU LỚN</h4>
                  <p>Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa, giúp đem lại cảm giác thoải mái, thư giãn, tăng cường giấc ngủ tự nhiên, ngon giấc cho bé. Duy trì giấc ngủ thoải mái, sâu giấc giúp bé lớn nhanh và khỏe mạnh.</p>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="sAbout__info__item">
                  <div class="icon"><img src="/images/aboutus/icon7.svg" alt=""></div>
                  <h4>CHỐNG TÁO BÓN</h4>
                  <p>Chất xơ hòa tan FOS giúp cân bằng hệ vi sinh đường ruột, cho hệ tiêu hóa luôn khỏe mạnh, đồng thời duy trì nhu động ruột, làm mềm phân và ngăn ngừa táo bón cho trẻ.</p>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="sAbout__info__item">
                  <div class="icon"><img src="/images/aboutus/icon8.svg" alt=""></div>
                  <h4>PHÁT TRIỂN CHIỀU CAO</h4>
                  <p>Canxi, Photpho, Vitamin D3: Dưỡng chất cần thiết cho sự phát triển của xương, răng để bé phát triển chiều dài cơ thể, xương răng cứng cáp, chắc khỏe và đạt được chiều cao vượt trội.</p>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="sAbout__info__item">
                  <div class="icon"><img src="/images/aboutus/icon9.svg" alt=""></div>
                  <h4>TĂNG CÂN KHỎE MẠNH</h4>
                  <p>Công thức dinh dưỡng hợp lý, cung cấp đầy đủ chất Đạm, chất Béo, Vitamin và Khoáng chất cần thiết cho quá trình phát triển cấu trúc và chức năng của cơ thể, giúp bé tăng cân khỏe mạnh.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="sProduct" data-waypoint="80%">
        <div class="container">
          <div class="heading-2">Sản phẩm</div>
          <div class="navTab"><a class="btn-nav" href="#">Navigation</a>
            <div class="mb-tab">
              <ul>
                <li><a class="active" href="#" data-filter="*">Tất cả    </a></li>
                <li><a href="#" data-filter=".fiter_tab1"> Từ 0 -12 tháng</a></li>
                <li><a href="#" data-filter=".fiter_tab2">Từ 1 -2 tuổi</a></li>
                <li><a href="#" data-filter=".fiter_tab3"> Trên 2 tuổi</a></li>
              </ul>
            </div>
          </div>
          <div class="sProduct__inner filtr-container sProductGird">
            <div class="sProduct__inner__item filter fiter_tab1">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product01.png)" data-toggle="modal" data-target="#popupNotify"><img src="/images/product/product01.png" alt=""></a></div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#" data-toggle="modal" data-target="#popupNotify">Colosbaby gold 0+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#" data-toggle="modal" data-target="#popupNotify">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab1">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product01.png)" data-toggle="modal" data-target="#popupNotify"><img src="/images/product/product01.png" alt=""></a></div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#" data-toggle="modal" data-target="#popupNotify">Colosbaby gold 1+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#" data-toggle="modal" data-target="#popupNotify">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab2">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product02.png)"><img src="/images/product/product02.png" alt=""></a>
                <!--+img-bg(image,'','',link)-->
              </div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#">Colosbaby gold 2+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab3">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product03.png)"><img src="/images/product/product03.png" alt=""></a>
                <!--+img-bg(image,'','',link)-->
              </div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#">Colosbaby 600 IgG 0+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab1">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product03.png)" data-toggle="modal" data-target="#popupNotify"><img src="/images/product/product03.png" alt=""></a></div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#" data-toggle="modal" data-target="#popupNotify">Colosbaby 600 IgG 1+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#" data-toggle="modal" data-target="#popupNotify">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab2">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product04.png)"><img src="/images/product/product04.png" alt=""></a>
                <!--+img-bg(image,'','',link)-->
              </div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#">Colosbaby 600 IgG 2+</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab3">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product04.png)"><img src="/images/product/product04.png" alt=""></a>
                <!--+img-bg(image,'','',link)-->
              </div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#">Colosbaby 0 -12 tháng</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#">Xem chi tiết</a>
            </div>
            <div class="sProduct__inner__item filter fiter_tab4">
              <div class="sProduct__inner__item__wrap"><a class="img-bg" href="#" style="background-image: url(/images/product/product05.png)"><img src="/images/product/product05.png" alt=""></a>
                <!--+img-bg(image,'','',link)-->
              </div>
              <div class="sProduct__inner__item__info">
                <h4><a href="#">Colosbaby 1 - 3 tuổi</a></h4>
                <p> Lượng kháng thể cao IgG1000mg giúp tăng cường miễn dịch. Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa giúp bé Ngủ ngon mau lớn
                </p>
              </div><a class="v-more" href="#">Xem chi tiết</a>
            </div>
          </div>
        </div>
      </section>
      <section class="sTvc" data-waypoint="80%">
        <div class="container"> 
          <div class="heading-2">TVC</div>
          <div class="sTvc__inner">
            <div class="sTvcLeft sTvcArrow">
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="sTvcMain">
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="sTvcRight sTvcArrow">
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sTvc__inner__item">
                <div class="image"><a href="#" data-toggle="modal" data-target="#popupTVC"></a><span class="ic-play"></span>
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/tvc/img.jpg)"><img src="/images/tvc/img.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="sTvc__arrow"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="sCertificate" data-waypoint="80%">
        <div class="container">
          <div class="heading-2">Chia sẻ của các mẹ</div>
          <div class="sCertificate__inner">
            <div class="sCertificateArrow sCertificateArrowLeft">
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img.png)"><img src="/images/ykien/img.png" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img1.jpg)"><img src="/images/ykien/img1.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img2.jpg)"><img src="/images/ykien/img2.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="sCertificateMain">
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img1.jpg)"><img src="/images/ykien/img1.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img2.jpg)"><img src="/images/ykien/img2.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img.png)"><img src="/images/ykien/img.png" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="sCertificateArrow sCertificateArrowRight">
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img2.jpg)"><img src="/images/ykien/img2.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img1.jpg)"><img src="/images/ykien/img1.jpg" alt="">
                    </div>
                  </div>
                </div>
              </div>
              <div class="sCertificate__inner__item">
                <div class="image">
                  <div class="imageInner">
                    <div class="img-bg" style="background-image: url(/images/ykien/img.png)"><img src="/images/ykien/img.png" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="sCertificate__arrow"></div>
            </div>
          </div>
          <div class="sCertificateInfo">
            <div class="sCertificateInfo__item active">
              <div class="info">
                <div class="name-mom">Mẹ Ngọc Hiền (TP HCM)</div>
                <div class="text-mom">Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh</div>
                <p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kháng thể tự nhiên từ sữa mẹ là tốt nhất cho con nên mình rất lo lắng nếu hệ miễn dịch của con không khoẻ mạnh. May mắn minh tìm được Colosbaby có thành phần sữa non, bé rất thích uống, ăn ngon, ngủ tốt. Nghé trộm vía gần như không ốm vặt suốt các năm đầu đời.</p>
              </div>
            </div>
            <div class="sCertificateInfo__item">
              <div class="info">
                <div class="name-mom">Mẹ Ngọc Hiền (TP HCM)</div>
                <div class="text-mom">Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh</div>
                <p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>
              </div>
            </div>
            <div class="sCertificateInfo__item">
              <div class="info">
                <div class="name-mom">Mẹ Ngọc Hiền (TP HCM)</div>
                <div class="text-mom">Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh</div>
                <p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="sNews" data-waypoint="80%">
        <div class="container">
          <div class="heading-3">Kiến Thức/ Truyền thông nói gì về chúng tôi</div>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"><a class="nav-link active" id="kienthuc-tab" data-toggle="tab" href="#kienthuc" role="tab" aria-controls="kienthuc" aria-selected="true">Kiến thức    </a></li>
            <li class="nav-item"><a class="nav-link" id="truyenthong-tab" data-toggle="tab" href="#truyenthong" role="tab" aria-controls="truyenthong" aria-selected="false"> Truyền thông nói gì về chúng tôi  </a></li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="kienthuc" role="tabpanel" aria-labelledby="kienthuc-tab">
              <div class="sNews__inner sNewsSlide">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Không cần  kháng sinh, mẹ vẫn tăng cường sức khỏe con nhờ kháng thể này</a>
                    <div class="des">Tận dụng hiệu quả nguồn kháng thể IgG tự nhiên có trong sữa non là mẹ đã giúp con tăng cường sức đề kháng, “nghỉ chơi” với kháng sinh.
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</a>
                    <div class="des">IgG là yếu tố quan trọng cho hệ miễn dịch của bé. IgG phản ứng lại với những “khách lạ” ghé thăm cơ thể chúng ta như vi khuẩn gây bệnh hoặc truyền bệnh
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</a>
                    <div class="des">IgG là yếu tố quan trọng cho hệ miễn dịch của bé. IgG phản ứng lại với những “khách lạ” ghé thăm cơ thể chúng ta như vi khuẩn gây bệnh hoặc truyền bệnh
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="text-center"><a class="btn-link-all" href="#"><span>Xem tất cả</span></a></div>
            </div>
            <div class="tab-pane fade" id="truyenthong" role="tabpanel" aria-labelledby="truyenthong-tab">
              <div class="sNews__inner sNewsSlide">
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Mẹ Việt trải lòng chuyện nuôi con không thành… thiên tài</a>
                    <div class="des">Con cái không phải là tấm huy chương để cha mẹ làm oách. Con phải xuất sắc, phải trường chuyên lớp chọn, phải giải thành phố, quốc gia rồi quốc tế để làm gì
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Không cần  kháng sinh, mẹ vẫn tăng cường sức khỏe con nhờ kháng thể này</a>
                    <div class="des">Tận dụng hiệu quả nguồn kháng thể IgG tự nhiên có trong sữa non là mẹ đã giúp con tăng cường sức đề kháng, “nghỉ chơi” với kháng sinh.
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
                <div class="sNews__inner__item" data-tags="news-filter">
                  <div class="img-bg" style="background-image: url(/images/news/news01.jpg)"><img src="/images/news/news01.jpg" alt="#">
                  </div>
                  <div class="info"><a href="#"> Sữa non - Giải pháp tăng cường hệ miễn dịch cho bé</a>
                    <div class="des">IgG là yếu tố quan trọng cho hệ miễn dịch của bé. IgG phản ứng lại với những “khách lạ” ghé thăm cơ thể chúng ta như vi khuẩn gây bệnh hoặc truyền bệnh
                    </div>
                    <div class="btnLink"><a href="#">Xem chi tiết </a></div>
                  </div>
                </div>
              </div>
              <div class="text-center"><a class="btn-link-all" href="#"><span>Xem tất cả</span></a></div>
            </div>
          </div>
        </div>
      </section>
      <section class="sNewsLetter" data-waypoint="80%">
        <div class="container">
          <h4 class="text-center">Đăng ký nhận ưu đãi</h4>
          <div class="newsLetter">
            <p>Nhập email của mẹ</p>
            <div class="row align-items-center">
              <div class="col-lg-9">
                <div class="newsLetter__ipt">
                  <input class="form-control" type="text" placeholder="">
                </div>
              </div>
              <div class="col-lg-3">
                <button class="btnSm"><span>Đăng ký ngay</span></button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="modal fade" id="popupTVC" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
              <div class="modal-body-video">
                <iframe width="100%" height="553" src="https://www.youtube.com/embed/BJwxhnbYVqY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Popup thong bao-->
      <div class="modal fade" id="popupNotify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
              <div class="modal-body-notify">
                <h4>Thông báo</h4>
                <ul> 
                  <li>Thông tin sản phẩm này chỉ dành cho nhân viên y tế</li>
                  <li>Thông tin chỉ dành cho người có nhu cầu tìm hiểu, nghiên cứu sản phẩm</li>
                  <li> Thông tin không mang tính chất quảng cáo sản phẩm</li>
                </ul>
                <div class="text-hightlight"><span>Vui lòng liên hệ Hotline                </span><span class="text-hotline">1900 636 958   </span><span>để được tư vấn thêm!</span></div>
                <div class="btnGroup">
                  <div class="row">
                    <div class="col-6">
                      <button class="btnSm btnCancle" type="button" data-dismiss="modal" aria-label="Close"><span> Bỏ qua</span></button>
                    </div>
                    <div class="col-6">
                      <button class="btnSm"><span> Xác nhận</span></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection