<!DOCTYPE html>
<html>
    <head>
        <title>CÔNG TY CỔ PHẦN SỮA VITADAIRY VIỆT NAM</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="/assets/images/icons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icons/favicon-16x16.png">
        <link rel="shortcut icon" href="/assets/images/icons/favicon.ico">
        <link rel="manifest" href="/assets/manifest.json">
        <meta name="msapplication-TileColor" content="#f05b28">
        <meta name="msapplication-TileImage" content="/assets/images/icons/ms-icon-144x144.png">
        <meta name="theme-color" content="#f05b28">
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link href="/assets/css/styles.css" rel="stylesheet">

        @yield('style')
    </head>

    <body>
        @include('frontend.layouts.header')

        <main class="wrapper">
            @yield('content')
        </main>

        @include('frontend.layouts.footer')

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="/assets/js/library.js"></script>
        <script src="/assets/js/main.js"></script>

        @yield('script')
    </body>
</html>