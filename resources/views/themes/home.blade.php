@extends('frontend.master')

@section('content')

<div class="sBanner" data-waypoint="100%">
    <div class="sBanner__inner">
        <div class="sBanner__item">
            <div class="img-bg" style="background-image: url('{{ isset($blocks['BANNER']) && isset($blocks['BANNER'][0]) ? $blocks['BANNER'][0]->photo : '' }}')">
                <img src="{{ isset($blocks['BANNER']) && isset($blocks['BANNER'][0]) ? $blocks['BANNER'][0]->photo : '' }}" alt="">
            </div>
            <div class="container">
                <div class="sBanner__item__img">
                    <img src="{!! isset($blocks['SUC-MANH-BEN-TRONG']) && isset($blocks['SUC-MANH-BEN-TRONG'][0]) ? $blocks['SUC-MANH-BEN-TRONG'][0]->photo : '' !!}" alt="{!! isset($blocks['SUC-MANH-BEN-TRONG']) && isset($blocks['SUC-MANH-BEN-TRONG'][0]) ? $blocks['SUC-MANH-BEN-TRONG'][0]->name : '' !!}">
                </div>
                <div class="sBanner__item__title">
                    <h1>{!! isset($blocks['TANG-CUONG-MIEN-DICH']) && isset($blocks['TANG-CUONG-MIEN-DICH'][0]) ? $blocks['TANG-CUONG-MIEN-DICH'][0]->name : '' !!}</h1>
                    <div class="image"> 
                        <img src="{!! isset($blocks['TANG-CUONG-MIEN-DICH']) && isset($blocks['TANG-CUONG-MIEN-DICH'][0]) ? $blocks['TANG-CUONG-MIEN-DICH'][0]->photo : '' !!}" alt="{!! isset($blocks['TANG-CUONG-MIEN-DICH']) && isset($blocks['TANG-CUONG-MIEN-DICH'][0]) ? $blocks['TANG-CUONG-MIEN-DICH'][0]->name : '' !!}">
                    </div>
                    <h3>{!! isset($blocks['SUC-MANH-BEN-TRONG']) && isset($blocks['SUC-MANH-BEN-TRONG'][0]) ? $blocks['SUC-MANH-BEN-TRONG'][0]->name : '' !!}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--su menh tam nhin-->
<section class="sImmune" id="tang-cuong-he-mien-dich" data-waypoint="80%">
    <div class="container">
        @if(isset($blocks['NUOI-CON-THONG-MINH']) && isset($blocks['NUOI-CON-THONG-MINH'][0]))
        <div class="heading text-center">
            <h2>{!! $blocks['NUOI-CON-THONG-MINH'][0]->name !!}</h2>
            {!! $blocks['NUOI-CON-THONG-MINH'][0]->content !!}
        </div>
        @endif
        <div class="sImmune__inner">
            <h2 class="text-center"> {!! isset($blocks['IGG-TANG-CUONG-MIEN-DICH']) && isset($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]) ? $blocks['IGG-TANG-CUONG-MIEN-DICH'][0]->name : '' !!}</h2>
        </div>
        <div class="sImmune__wrap">
            <div class="image"><img src="{!! isset($blocks['IGG-TANG-CUONG-MIEN-DICH']) && isset($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]) ? $blocks['IGG-TANG-CUONG-MIEN-DICH'][0]->photo : '' !!}" alt="{!! isset($blocks['IGG-TANG-CUONG-MIEN-DICH']) && isset($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]) ? $blocks['IGG-TANG-CUONG-MIEN-DICH'][0]->name : '' !!}"></div>
            @if(isset($blocks['IGG-TANG-CUONG-MIEN-DICH']) && isset($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]) && isset($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]->children))
            @foreach($blocks['IGG-TANG-CUONG-MIEN-DICH'][0]->children as $key => $block)
            <div class="sImmune__item positionAb-{{ $key + 1 }}">
                <div class="icon"><img src="{{ $block->icon }}" alt=""></div>
                <p>{!! $block->description !!}</p>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<section class="sAbout" id="bo-sung-khang-the" data-waypoint="80%">
    <div class="container">
        @if(isset($blocks['COLORS-BABY-LOGO']) && isset($blocks['COLORS-BABY-LOGO'][0]))
        <div class="sAbout__inner">
            <img src="{{ $blocks['COLORS-BABY-LOGO'][0]->photo }}" alt="{!! $blocks['COLORS-BABY-LOGO'][0]->name !!}">
            <h4>{!! $blocks['COLORS-BABY-LOGO'][0]->name !!}</h4>
        </div>
        @endif
        <div class="sAbout__content">
            <div class="sAbout__wrap">
                <div class="image"><img src="{{ isset($blocks['IGG-IMAGE-CENTER']) && isset($blocks['IGG-IMAGE-CENTER'][0]) ? $blocks['IGG-IMAGE-CENTER'][0]->photo : '' }}" alt=""></div>
                @if(isset($blocks['IGG-IMAGE-CENTER']) && isset($blocks['IGG-IMAGE-CENTER'][0]) && isset($blocks['IGG-IMAGE-CENTER'][0]->children))
                @foreach($blocks['IGG-IMAGE-CENTER'][0]->children as $key => $block)
                @if($key == 0)
                <div class="sAbout__item positionAb-5">
                <div class="icon"><img src="{{ $block->icon }}" alt="{!! isset($block->name) ? $block->name : '' !!}"></div>
                    <div class="info">
                        <h4>{!! isset($block->name) ? $block->name : '' !!}</h4>
                        <p>{!! $block->description !!}</p>
                    </div>
                </div>
                @else
                <div class="sAbout__item positionAb-{{ $key }}">
                    <div class="icon"><img src="{{ $block->icon }}" alt="{!! $block->description !!}"></div>
                    <div class="info">
                        <p>{!! $block->description !!} </p>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
        </div>
        <div class="sAbout__info">
            <div class="row">
                @if(isset($blocks['COLORS-BABY-CONG-DUNG']) && isset($blocks['COLORS-BABY-CONG-DUNG'][0]) && isset($blocks['COLORS-BABY-CONG-DUNG'][0]->children))
                @foreach($blocks['COLORS-BABY-CONG-DUNG'][0]->children as $key => $block)
                <div class="col-lg-3 col-sm-6">
                    <div class="sAbout__info__item">
                    <div class="icon"><img src="{{ $block->icon }}" alt="{!! $block->name !!}"></div>
                        <h4>{!! $block->name !!}</h4>
                        <p>{!! $block->description !!}</p>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
<section class="sProduct" data-waypoint="80%">
    <div class="container">
        <div class="heading-2">{!! isset($blocks['SAN-PHAM']) && isset($blocks['SAN-PHAM'][0]) ? $blocks['SAN-PHAM'][0]->name : '' !!}</div>
        <div class="navTab"><a class="btn-nav" href="#">Navigation</a>
            <div class="mb-tab">
                <ul>
                    <li><a class="active" href="#" data-filter="*">Tất cả </a></li>
                    @if(isset($product_category))
                    @foreach($product_category as $category)
                    <li><a href="#" data-filter=".{{ str_slug($category->name) }}"> {{ $category->name }}</a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <div class="sProduct__inner filtr-container sProductGird">
            @if(isset($product))
            @foreach($product as $value)
            <div class="sProduct__inner__item filter {{ str_slug($value->category->name) }}">
            <div class="sProduct__inner__item__wrap"><a class="img-bg" href="{{ $value->url }}" style="background-image: url('{{ $value->image }}')"
            @if($value->is_popup == 1)data-toggle="modal" data-target="#popupNotify"@endif><img src="{{ $value->image }}" alt=""></a></div>
                <div class="sProduct__inner__item__info">
                <h4><a href="{{ $value->is_popup == 0 ? $block->url : '#' }}" @if($value->is_popup == 1)data-toggle="modal" data-target="#popupNotify"@endif>{{ $value->name }}</a></h4>
                    <p> {{ $value->description }}
                    </p>
                </div><a class="v-more" href="{{ $value->is_popup == 0 ? $block->url : '#' }}" @if($value->is_popup == 1)data-toggle="modal" data-target="#popupNotify"@endif>Xem chi tiết</a>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<section class="sTvc" data-waypoint="80%">
    <div class="container">
    <div class="heading-2">{!! isset($blocks['TVC']) && isset($blocks['TVC'][0]) ? $blocks['TVC'][0]->name : '' !!}</div>
        <div class="sTvc__inner">
            <div class="sTvcLeft sTvcArrow">
                @if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
                @foreach($blocks['TVC'][0]->children as $key => $block)
                <div class="sTvc__inner__item">
                    <div class="image">
                        <a href="#" data-toggle="modal" data-target="#popupTVC-{{ $key }}"></a>
                        <span class="ic-play"></span>
                        <div class="imageInner">
                            <div class="img-bg" style="background-image: url('{{ $block->photo }}')">
                                <img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="sTvcMain">
                @if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
                @foreach(swapCollection($blocks['TVC'][0]->children, 1) as $key => $block)
                <div class="sTvc__inner__item">
                    <div class="image">
                        <a href="#" data-toggle="modal" data-target="#popupTVC1-{{ $key }}"></a>
                        <span class="ic-play"></span>
                        <div class="imageInner">
                            <div class="img-bg" style="background-image: url('{{ $block->photo }}')">
                                <img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="sTvcRight sTvcArrow">
                @if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
                @foreach(swapCollection($blocks['TVC'][0]->children, 2) as $key => $block)
                <div class="sTvc__inner__item">
                    <div class="image">
                        <a href="#" data-toggle="modal" data-target="#popupTVC2-{{ $key }}"></a>
                        <span class="ic-play"></span>
                        <div class="imageInner">
                            <div class="img-bg" style="background-image: url('{{ $block->photo }}')">
                                <img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="container">
                <div class="sTvc__arrow"></div>
            </div>
        </div>
    </div>
</section>
<section class="sCertificate" data-waypoint="80%">
    <div class="container">
        <div class="heading-2">{!! isset($blocks['CHIA-SE-CUA-CAC-ME']) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]) ? $blocks['CHIA-SE-CUA-CAC-ME'][0]->name : '' !!}</div>
        <div class="sCertificate__inner">
            <div class="sCertificateArrow sCertificateArrowLeft">
                @if(isset($blocks['CHIA-SE-CUA-CAC-ME']) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]->children))
                @foreach($blocks['CHIA-SE-CUA-CAC-ME'][0]->children as $key => $block)
                <div class="sCertificate__inner__item">
                    <div class="image">
                        <div class="imageInner">
                        <div class="img-bg" style="background-image: url('{{ $block->photo }}')"><img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="sCertificateMain">
                @if(isset($blocks['CHIA-SE-CUA-CAC-ME']) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]->children))
                @foreach(swapCollection($blocks['CHIA-SE-CUA-CAC-ME'][0]->children, 1) as $key => $block)
                <div class="sCertificate__inner__item">
                    <div class="image">
                        <div class="imageInner">
                            <div class="img-bg" style="background-image: url('{{ $block->photo }}')"><img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="sCertificateArrow sCertificateArrowRight">
                @if(isset($blocks['CHIA-SE-CUA-CAC-ME']) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]->children))
                @foreach(swapCollection($blocks['CHIA-SE-CUA-CAC-ME'][0]->children, 2) as $key => $block)
                <div class="sCertificate__inner__item">
                    <div class="image">
                        <div class="imageInner">
                            <div class="img-bg" style="background-image: url('{{ $block->photo }}')"><img src="{{ $block->photo }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="container">
                <div class="sCertificate__arrow"></div>
            </div>
        </div>
        <div class="sCertificateInfo">
            @if(isset($blocks['CHIA-SE-CUA-CAC-ME']) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]) && isset($blocks['CHIA-SE-CUA-CAC-ME'][0]->children))
            @foreach(swapCollection($blocks['CHIA-SE-CUA-CAC-ME'][0]->children, 1) as $key => $block)
            <div class="sCertificateInfo__item {{ $key == 0 ? 'active' : '' }}">
                <div class="info">
                    <div class="name-mom">{!! $block->name !!}</div>
                    <div class="text-mom">{!! $block->description !!}</div>
                    {!! $block->content !!}
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<section class="sNews" data-waypoint="80%">
    <div class="container">
        <div class="heading-3">Kiến Thức/ Truyền thông nói gì về chúng tôi</div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @if(isset($new_category))
            @foreach($new_category as $key => $category)
                <li class="nav-item"><a class="nav-link {{ $key == 0 ? 'active' : '' }}" id="{{ $category->slug }}-tab" data-toggle="tab" href="#{{ $category->slug }}" role="tab"
                    aria-controls="{{ $category->slug }}" aria-selected="true">{{ $category->name }} </a></li>
            @endforeach
            @endif
        </ul>
        <div class="tab-content" id="myTabContent">
            @if(isset($new_category))
            @foreach($new_category as $key => $category)
            <div class="tab-pane fade {{ $key == 0 ? 'show active' : '' }}" id="{{ $category->slug }}" role="tabpanel" aria-labelledby="{{ $category->slug }}-tab">
                @if(isset($category->news))
                <div class="sNews__inner sNewsSlide">
                    @foreach($category->news->where('active', 1) as $new)
                    <div class="sNews__inner__item" data-tags="news-filter">
                        <div class="img-bg" style="background-image: url('{{ $new->image }}')"><img src="{{ $new->image }}" alt="#">
                        </div>
                        <div class="info"><a href="{{ route('new.detail.get', ['new_category_slug' => $category->slug, 'new_slug' => $new->slug]) }}"> {{ $new->title }}</a>
                            <div class="des">{!! $new->description !!}</div>
                            <div class="btnLink"><a href="{{ route('new.detail.get', ['new_category_slug' => $category->slug, 'new_slug' => $new->slug]) }}">Xem chi tiết </a></div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center"><a class="btn-link-all" href="{{ route('new.category.get', ['new_category_slug' => $category->slug]) }}"><span>Xem tất cả</span></a></div>
                @endif
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<section class="sNewsLetter" data-waypoint="80%">
    <div class="container">
        <h4 class="text-center">Đăng ký nhận ưu đãi</h4>
        <div class="newsLetter">
            <p>Nhập email của mẹ</p>
            <form id="emailRegister" action="{{ route('email.register.post') }}" method="POST">
                {{ csrf_field() }}
                <div class="row align-items-center">
                    <div class="col-lg-9">
                        <div class="newsLetter__ipt">
                            <input class="form-control" name="email" type="email" placeholder="" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <button class="btnSm" type="submit"><span>Đăng ký ngay</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
@foreach($blocks['TVC'][0]->children as $key => $block)
<div class="modal fade" id="popupTVC-{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
                <div class="modal-body-video">
                    <iframe width="100%" height="553" src="{{ $block->url }}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

@if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
@foreach(swapCollection($blocks['TVC'][0]->children, 1) as $key => $block)
<div class="modal fade" id="popupTVC1-{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
                <div class="modal-body-video">
                    <iframe width="100%" height="553" src="{{ $block->url }}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

@if(isset($blocks['TVC']) && isset($blocks['TVC'][0]) && isset($blocks['TVC'][0]->children))
@foreach(swapCollection($blocks['TVC'][0]->children, 1) as $key => $block)
<div class="modal fade" id="popupTVC2-{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
                <div class="modal-body-video">
                    <iframe width="100%" height="553" src="{{ $block->url }}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

<!-- Popup thong bao -->
<div class="modal fade" id="popupNotify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
                <div class="modal-body-notify">
                    <h4>Thông báo</h4>
                    <ul>
                        <li>Thông tin sản phẩm này chỉ dành cho nhân viên y tế</li>
                        <li>Thông tin chỉ dành cho người có nhu cầu tìm hiểu, nghiên cứu sản phẩm</li>
                        <li> Thông tin không mang tính chất quảng cáo sản phẩm</li>
                    </ul>
                    <div class="text-hightlight"><span>Vui lòng liên hệ Hotline </span><span class="text-hotline">1900
                            636 958 </span><span>để được tư vấn thêm!</span></div>
                    <div class="btnGroup">
                        <div class="row">
                            <div class="col-6">
                                <button class="btnSm btnCancle" type="button" data-dismiss="modal" aria-label="Close"><span>
                                        Bỏ qua</span></button>
                            </div>
                            <div class="col-6">
                                <button class="btnSm"><span> Xác nhận</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Popup thong bao dang ky email thanh cong -->
<div class="modal fade" id="popupEmailRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="icon_close_alt2"></i></button>
            <div class="modal-body">
                <div class="modal-body-notify">
                    <h4>Đăng ký thành công.</h4>

                    <div class="text-hightlight">
                        <span>Vui lòng liên hệ Hotline </span>
                        <span class="text-hotline">1900 636 958 </span>
                        <span>để được tư vấn thêm!</span>
                    </div>
                    <div class="btnGroup">
                        <div class="row">
                            <div class="col-6">
                                <button class="btnSm btnCancle" type="button" data-dismiss="modal" aria-label="Close">
                                    <span>Bỏ qua</span>
                                </button>
                            </div>
                            <div class="col-6">
                                <button class="btnSm"><span> Xác nhận</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        $( document ).ready(function() {

            $('#emailRegister').submit(function(e) {
                e.preventDefault();

                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: url,
                    data: form.serialize(),
                    success: function(data) {
                        console.log(data);
                        $('#emailRegister')[0].reset();
                        $('#popupEmailRegister').modal('show');
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endsection