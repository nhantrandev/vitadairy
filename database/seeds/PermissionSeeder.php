<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Permission::truncate();

        $permissions  = config("permission");

        foreach ($permissions as $permission){
            foreach ($permission["permissions"] as $key => $name){
                $check = Permission::where("slug", $key)->first();
                if(!$check){
                    $arr = [
                        "model" => $permission["model"],
                        "slug" => $key,
                        "name" => $name,
                        "description" => $name
                    ];
                    Permission::create($arr);
                }
            }
        }

        // Remove cache permission
        removeAllConfig();
    }
}
