<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\PageBlock;
use App\Models\PageBlockTranslation;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::truncate();
        PageTranslation::truncate();
        PageBlock::truncate();
        PageBlockTranslation::truncate();

        $locales = config("laravellocalization.supportedLocales"); 
        
        $pages = [
            // Home page
            [
                'parent_id' => 0,
                'group_code' => 'HOME',
                'code' => 'HOME',
                'theme' => 'home',
                'active' => 1,
                'title' => "Home",
                'content' => '',
                'slug' => '/',
                'blocks' =>[
                    [
                        "parent_id" => "",
                        "code" => "BANNER",
                        "types" => ["TYPE_PHOTO"],
                        "values" => [
                            '/images/slider/banner1.jpg'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "TANG-CUONG-MIEN-DICH",
                        "types" => ["TYPE_NAME", "TYPE_PHOTO"],
                        "values" => [
                            'TĂNG CƯỜNG <br> MIỄN DỊCH',
                            '/images/slider/khangthetusuanon.png',
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "SUC-MANH-BEN-TRONG",
                        "types" => ["TYPE_NAME", "TYPE_PHOTO"],
                        "values" => [
                            'SỨC MẠNH BÊN TRONG',
                            '/images/slider/colosbaby.png',
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "NUOI-CON-THONG-MINH",
                        "types" => ["TYPE_NAME", "TYPE_CONTENT"],
                        "values" => [
                            'NUÔI CON THÔNG MINH LÀ NUÔI KỲ VỌNG CỦA MẸ CHO CON MIỄN DỊCH KHOẺ MỚI LÀ VÌ CON !!',
                            '<p>Mẹ có biết, hai năm đầu đời là khung thời gian quan trọng cho sự phát triển và nuôi dưỡng hệ miễn dịch của trẻ, hệ miễn dịch khỏe mạnh không chỉ có ý nghĩa trong những năm tháng đầu đời mà còn tác động lâu dài đến trẻ, có thể suốt cuộc đời. Nuôi dưỡng hệ miễn dịch khoẻ, là nuôi dưỡng cho con nền tảng bên trong hoàn thiện , giúp trẻ khỏe mạnh để học hỏi tốt hơn, từ đó phát triển tối ưu về mặt trí tuệ để sẵn sàng đón nhận nhiều cơ hội trong tương lai.</p>'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "IGG-TANG-CUONG-MIEN-DICH",
                        "types" => ["TYPE_NAME", "TYPE_PHOTO"],
                        "values" => [
                            ' IgG TĂNG CƯỜNG MIỄN DỊCH <br>CHỐNG BỆNH VẶT',
                            '/images/aboutus/tang-cuong-he-mien-dich.png'
                        ]
                    ],
                        [
                            "parent_id" => "IGG-TANG-CUONG-MIEN-DICH",
                            "code" => "IGG-TANG-CUONG-MIEN-DICH-1",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/virut.png',
                                'IgG bám dính vào màng của Virus, vi khuẩn, đánh dấu để tế bào miễn dịch nhận diện và tiêu diệt'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-TANG-CUONG-MIEN-DICH",
                            "code" => "IGG-TANG-CUONG-MIEN-DICH-2",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/khangthe.png',
                                'Immunoglobulin G (IgG) là loại kháng thể chiếm khoảng 75% tổng kháng thể có trong huyết thanh và dịch ngoại tế bào'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-TANG-CUONG-MIEN-DICH",
                            "code" => "IGG-TANG-CUONG-MIEN-DICH-3",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/ditruyen.png',
                                'IgG được truyền từ mẹ sang con qua sữa mẹ, đặc biệt quan trọng trong “khoảng trống miễn dịch” từ 0-36 tháng tuổi'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-TANG-CUONG-MIEN-DICH",
                            "code" => "IGG-TANG-CUONG-MIEN-DICH-4",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/bosung.png',
                                'IgG cần được bổ sung vào sữa đủ hàm lượng mới tạo ra khả năng miễn dịch hiệu quả'
                            ]
                        ],
                    [
                        "parent_id" => "",
                        "code" => "COLORS-BABY-LOGO",
                        "types" => ["TYPE_PHOTO", "TYPE_NAME"],
                        "values" => [
                            '/images/aboutus/colosbaby.png',
                            'Bổ sung kháng thể IgG tự nhiên từ sữa non. Cho con sức mạnh bên trong'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "IGG-IMAGE-CENTER",
                        "types" => ["TYPE_NAME", "TYPE_PHOTO"],
                        "values" => [
                            'Hình ảnh IgG giữa',
                            '/images/aboutus/cows.png'
                        ]
                    ],
                        [
                            "parent_id" => "IGG-IMAGE-CENTER",
                            "code" => "IGG-CONTENT-BOTTOM",
                            "types" => ["TYPE_ICON", "TYPE_NAME", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon5.svg',
                                'TĂNG CƯỜNG MIỄN DỊCH  SỨC ĐỀ KHÁNG HIỆU QUẢ',
                                'Bổ sung kháng thể cao IgG từ sữa non là cách trực tiếp để cơ thể bé có lượng IgG cao ổn định. Luôn sẵn sàng bảo vệ bé khỏi các tác nhân gây bệnh, giúp bé khoẻ mạnh và ít ốm vặt'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-IMAGE-CENTER",
                            "code" => "IGG-CONTENT-LEFT-1",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon1.svg',
                                'Giảm tỷ lệ số lần nhiễm khuẩn hô hấp trên: 56%                    '
                            ]
                        ],
                        [
                            "parent_id" => "IGG-IMAGE-CENTER",
                            "code" => "IGG-CONTENT-LEFT-2",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon2.svg',
                                'Giảm tỷ lệ số lần tiêu chảy: 39%'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-IMAGE-CENTER",
                            "code" => "IGG-CONTENT-RIGHT-1",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon3.svg',
                                'Giảm tỷ lệ số lần nhiễm khuẩn khác: 42%'
                            ]
                        ],
                        [
                            "parent_id" => "IGG-IMAGE-CENTER",
                            "code" => "IGG-CONTENT-RIGHT-2",
                            "types" => ["TYPE_ICON", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon4.svg',
                                'Giảm số lần nhập viện do các nguyên nhân trên 59%'
                            ]
                        ],
                    [
                        "parent_id" => "",
                        "code" => "COLORS-BABY-CONG-DUNG",
                        "types" => ["TYPE_NAME"],
                        "values" => [
                            '4 Công dụng của sản phẩm Colors Baby'
                        ]
                    ],
                        [
                            "parent_id" => "COLORS-BABY-CONG-DUNG",
                            "code" => "COLORS-BABY-CONG-DUNG-1",
                            "types" => ["TYPE_ICON", "TYPE_NAME", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon6.svg',
                                'NGỦ NGON MAU LỚN',
                                'Lactium – dưỡng chất đặc biệt của quá trình thủy phân casein sữa, giúp đem lại cảm giác thoải mái, thư giãn, tăng cường giấc ngủ tự nhiên, ngon giấc cho bé. Duy trì giấc ngủ thoải mái, sâu giấc giúp bé lớn nhanh và khỏe mạnh.'
                            ]
                        ],
                        [
                            "parent_id" => "COLORS-BABY-CONG-DUNG",
                            "code" => "COLORS-BABY-CONG-DUNG-2",
                            "types" => ["TYPE_ICON", "TYPE_NAME", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon7.svg',
                                'CHỐNG TÁO BÓN',
                                'Chất xơ hòa tan FOS giúp cân bằng hệ vi sinh đường ruột, cho hệ tiêu hóa luôn khỏe mạnh, đồng thời duy trì nhu động ruột, làm mềm phân và ngăn ngừa táo bón cho trẻ.'
                            ]
                        ],
                        [
                            "parent_id" => "COLORS-BABY-CONG-DUNG",
                            "code" => "COLORS-BABY-CONG-DUNG-3",
                            "types" => ["TYPE_ICON", "TYPE_NAME", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon8.svg',
                                'PHÁT TRIỂN CHIỀU CAO',
                                'Canxi, Photpho, Vitamin D3: Dưỡng chất cần thiết cho sự phát triển của xương, răng để bé phát triển chiều dài cơ thể, xương răng cứng cáp, chắc khỏe và đạt được chiều cao vượt trội.'
                            ]
                        ],
                        [
                            "parent_id" => "COLORS-BABY-CONG-DUNG",
                            "code" => "COLORS-BABY-CONG-DUNG-4",
                            "types" => ["TYPE_ICON", "TYPE_NAME", "TYPE_DESCRIPTION"],
                            "values" => [
                                '/images/aboutus/icon9.svg',
                                'TĂNG CÂN KHỎE MẠNH',
                                'Công thức dinh dưỡng hợp lý, cung cấp đầy đủ chất Đạm, chất Béo, Vitamin và Khoáng chất cần thiết cho quá trình phát triển cấu trúc và chức năng của cơ thể, giúp bé tăng cân khỏe mạnh.'
                            ]
                        ],
                    [
                        "parent_id" => "",
                        "code" => "SAN-PHAM",
                        "types" => ["TYPE_NAME"],
                        "values" => [
                            'Sản phẩm'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "TVC",
                        "types" => ["TYPE_NAME"],
                        "values" => [
                            'TVC'
                        ]
                    ],
                        [
                            "parent_id" => "TVC",
                            "code" => "VIDEO-YOUTUBE-1",
                            "types" => ["TYPE_PHOTO", "TYPE_URL"],
                            "values" => [
                                '/images/tvc/img.jpg',
                                'https://www.youtube.com/embed/BJwxhnbYVqY'
                            ]
                        ],
                        [
                            "parent_id" => "TVC",
                            "code" => "VIDEO-YOUTUBE-2",
                            "types" => ["TYPE_PHOTO", "TYPE_URL"],
                            "values" => [
                                '/images/tvc/img.jpg',
                                'https://www.youtube.com/embed/BJwxhnbYVqY'
                            ]
                        ],
                        [
                            "parent_id" => "TVC",
                            "code" => "VIDEO-YOUTUBE-3",
                            "types" => ["TYPE_PHOTO", "TYPE_URL"],
                            "values" => [
                                '/images/tvc/img.jpg',
                                'https://www.youtube.com/embed/BJwxhnbYVqY'
                            ]
                        ],
                        [
                            "parent_id" => "TVC",
                            "code" => "VIDEO-YOUTUBE-3",
                            "types" => ["TYPE_PHOTO", "TYPE_URL"],
                            "values" => [
                                '/images/tvc/img.jpg',
                                'https://www.youtube.com/embed/BJwxhnbYVqY'
                            ]
                        ],
                    [
                        "parent_id" => "",
                        "code" => "CHIA-SE-CUA-CAC-ME",
                        "types" => ["TYPE_NAME"],
                        "values" => [
                            'Chia sẻ của các mẹ'
                        ]
                    ],
                        [
                            "parent_id" => "CHIA-SE-CUA-CAC-ME",
                            "code" => "CHIA-SE-CUA-CAC-ME-1",
                            "types" => ["TYPE_PHOTO", "TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_CONTENT"],
                            "values" => [
                                '/images/ykien/img.png',
                                'Mẹ Ngọc Hiền (TP HCM)',
                                'Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh',
                                '<p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>'
                            ]
                        ],
                        [
                            "parent_id" => "CHIA-SE-CUA-CAC-ME",
                            "code" => "CHIA-SE-CUA-CAC-ME-2",
                            "types" => ["TYPE_PHOTO", "TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_CONTENT"],
                            "values" => [
                                '/images/ykien/img1.jpg',
                                'Mẹ Ngọc Hiền (TP HCM)',
                                'Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh',
                                '<p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kháng thể tự nhiên từ sữa mẹ là tốt nhất cho con nên mình rất lo lắng nếu hệ miễn dịch của con không khoẻ mạnh. May mắn minh tìm được Colosbaby có thành phần sữa non, bé rất thích uống, ăn ngon, ngủ tốt. Nghé trộm vía gần như không ốm vặt suốt các năm đầu đời.</p>'
                            ]
                        ],
                        [
                            "parent_id" => "CHIA-SE-CUA-CAC-ME",
                            "code" => "CHIA-SE-CUA-CAC-ME-3",
                            "types" => ["TYPE_PHOTO", "TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_CONTENT"],
                            "values" => [
                                '/images/ykien/img.png',
                                'Mẹ Ngọc Hiền (TP HCM)',
                                'Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh',
                                '<p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>'
                            ]
                        ],
                        [
                            "parent_id" => "CHIA-SE-CUA-CAC-ME",
                            "code" => "CHIA-SE-CUA-CAC-ME-4",
                            "types" => ["TYPE_PHOTO", "TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_CONTENT"],
                            "values" => [
                                '/images/ykien/img.png',
                                'Mẹ Ngọc Hiền (TP HCM)',
                                'Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh',
                                '<p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>'
                            ]
                        ],
                        [
                            "parent_id" => "CHIA-SE-CUA-CAC-ME",
                            "code" => "CHIA-SE-CUA-CAC-ME-5",
                            "types" => ["TYPE_PHOTO", "TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_CONTENT"],
                            "values" => [
                                '/images/ykien/img.png',
                                'Mẹ Ngọc Hiền (TP HCM)',
                                'Colosbaby giúp con mình có hệ miễn dịch khoẻ mạnh',
                                '<p>Sau khi sinh Nghé được 4 tháng thì mình bị mất sữa vì phải đi làm sớm, vì mình biết kh</p>'
                            ]
                        ],
                    [
                        "parent_id" => "",
                        "code" => "KIEN-THUC-TRUYEN-THONG",
                        "types" => ["TYPE_NAME", "TYPE_URL"],
                        "values" => [
                            'Kiến Thức/ Truyền thông nói gì về chúng tôi',
                            '#'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "DANG-KY-NHAN-UU-DAI",
                        "types" => ["TYPE_NAME"],
                        "values" => [
                            'Kiến Thức/ Truyền thông nói gì về chúng tôi',
                            '#'
                        ]
                    ],
                    [
                        "parent_id" => "",
                        "code" => "HINH-ANH-FOOTER",
                        "types" => ["TYPE_PHOTO"],
                        "values" => [
                            '../images/icons/cobe.png'
                        ]
                    ],
                ]
            ],
            // End home page
        ];

        foreach($pages as $page){
            $new_page = array(
                "parent_id" => $page["parent_id"],
                "group_code" => $page["group_code"],
                "code" => $page["code"],
                'theme' => $page["theme"],
                'active' => $page["active"]
            );

            foreach($locales as $key => $value){
                $new_page[$key]['title'] = $page["title"];
                $new_page[$key]['content'] = $page["content"];
                $new_page[$key]['slug'] = $page["slug"];
            }

            $file = resource_path("views/themes/".$page['theme'].".blade.php");

            if(!is_file($file))
                file_put_contents($file, '');

            $model = Page::create($new_page);

            if (isset($page['blocks']))
            {
                $this->createBlocks($model, $page['blocks']);
            }
        }
    }

    private function createBlocks($model, $blocks)
    {
        $locales = config("laravellocalization.supportedLocales"); 
        $position = 0;
        $parent_position = 0;

        foreach ($blocks as $key => $value) {

            $value['page_id'] = $model->id;
            
            if($value['parent_id']) {
                $value['parent_id'] = $this->findParentByCode($model, $value['parent_id']);
            } else{
                unset($value['parent_id']);
            }

            $types = $value['types'];
            $value['types'] = json_encode($value['types']);


            if($key >> 0) {
                if((empty($blocks[$key-1]['parent_id']) && !empty($blocks[$key]['parent_id']) )) {
                    $parent_position = $position;
                    $position = 0;
                }
                if((!empty($blocks[$key-1]['parent_id']) && empty($blocks[$key]['parent_id']) )) {
                    $position = $parent_position;
                }
            }

            $value['position'] = $position;

            if(!empty($types[0])) {
                foreach($locales as $key2 => $value2) {
                    foreach($types as $key3 => $type) {

                        if($type == 'TYPE_URL'){
                            $prop = 'url';
                        }else if($type == 'TYPE_NAME'){
                            $prop = 'name';
                        }else if($type == 'TYPE_DESCRIPTION'){
                            $prop = 'description';
                        }else if($type == 'TYPE_CONTENT'){
                            $prop = 'content';
                        }else if($type == 'TYPE_ICON'){
                            $value['icon'] = $value["values"][$key3];
                        }else if($type == 'TYPE_PHOTO'){
                            $value['photo'] = $value["values"][$key3];
                            continue;
                        }else if($type == 'TYPE_PHOTO_TRANSLATION'){
                            $prop = 'photo_translation';
                        }

                        $value[$key2][$prop] = $value["values"][$key3];

                    }
                }
            }

            unset($value['values']);


            $block = $model->blocks()->create($value);

            // Insert multi photo
            if (in_array(PageBlock::TYPE_MULTI_PHOTOS, $block->decode_types)) {
                if (!empty($value['photos'])) {
                    $block->createMedia($value['photos']);
                }
            }

            $position++;
        }
    }

    private function findParentByCode($model, $code) {
        return PageBlock::where('page_id', $model->id)->where('code', $code)->first()->id;
    }
}
