<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::truncate();

        $arr = [
            'name' => 'admin',
            'slug' => 'admin',
            'description' => 'Admin', 
            'level' => 100,
        ];

        $role = Role::create($arr);
        $role_permissions = '*';

        if($role_permissions == '*')
        {
            $permissions = Permission::get()->pluck("id")->toArray();
        }else
        {
            $permissions = Permission::whereIn('slug', $role_permissions)->get()->pluck("id")->toArray();
        }
        
        $role->syncPermissions($permissions);

    }
}
