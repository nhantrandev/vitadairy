<?php

use Illuminate\Database\Seeder;

use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\PageBlock;
use App\Models\PageBlockTranslation;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LocationDataSeeder::class);

        $this->call(PageSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
