<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewCategoryTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_category_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('new_category_id')->unsigned();
            $table->string('locale')->index();

            $table->string('name')->nullable();
            $table->string('slug')->nullable();

            $table->unique(['new_category_id','locale']);
			$table->foreign('new_category_id')->references('id')->on('new_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_category_translation');
    }
}
