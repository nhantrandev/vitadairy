<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#Repostories
use App\Repositories\EmailRepository;

class EmailController extends Controller
{

    protected $email;

    public function __construct(EmailRepository $email)
    {
        $this->email = $email;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Danh sách email');
        return view('admin.email.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JSON
     */
    public function datatable()
    {
        $data = $this->email->datatable();

        return DataTables::of($data)
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                            'link_delete' => route('admin.email.destroy', $data->id),
                            'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->email->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'email']));

        return redirect()->back();
    }
}
