<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\NewCategory;

#Repostories
use App\Repositories\NewRepository;

class NewController extends Controller
{

    protected $new;

    public function __construct(NewRepository $new)
    {
        $this->new = $new;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Danh sách tin tức');
        return view('admin.new.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JSON
     */
    public function datatable()
    {
        $data = $this->new->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->title;
                }
            )
             ->addColumn(
                'category',
                function ($data) {
                    return isset($data->category) ? $data->category->name : '';
                }
            )
            ->editColumn(
                'active',
                function ($data) {
                    return $data->active ? '<span class="label label-success">'.$data->label_active.'</span>' : '<span class="label label-warning">'.$data->label_active.'</span>';
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                        'link_edit' => route('admin.new.edit', $data->id),
                        'link_delete' => route('admin.new.destroy', $data->id),
                        'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Tạo tin tức');

        $new_category = NewCategory::get();

        return view('admin.new.create_edit', compact('new_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->new->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'tin tức']));

        return redirect()->route('admin.new.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Chỉnh sửa tin tức');

        $new = $this->new->find($id);

        $new_category = NewCategory::get();

        $metadata = $new->meta;

        return view('admin.new.create_edit', compact('new', 'new_category', 'metadata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->new->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'tin tức']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->new->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'tin tức']));

        return redirect()->back();
    }
}
