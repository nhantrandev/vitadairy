<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#Models
use App\Models\NewCategory;

#Repostories
use App\Repositories\NewCategoryRepository;

class NewCategoryController extends Controller
{

    protected $new_category;

    public function __construct(NewCategoryRepository $new_category)
    {
        $this->new_category = $new_category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Danh mục tin tức');
        return view('admin.new_category.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JSON
     */
    public function datatable()
    {
        $data = $this->new_category->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                        'link_edit' => route('admin.new.category.edit', $data->id),
                        'link_delete' => route('admin.new.category.destroy', $data->id),
                        'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Tạo danh mục');

        $parents = $this->new_category->all();

        return view('admin.new_category.create_edit', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->new_category->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'danh mục tin tức']));

        return redirect()->route('admin.new.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Chỉnh sửa danh mục');

        $new_category = $this->new_category->find($id);

        return view('admin.new_category.create_edit', compact('new_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->new_category->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'danh mục tin tức']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->new_category->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'danh mục tin tức']));

        return redirect()->back();
    }
}
