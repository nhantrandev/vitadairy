<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#Models
use App\Models\ProductCategory;

#Repostories
use App\Repositories\ProductCategoryRepository;

class ProductCategoryController extends Controller
{

    protected $product_category;

    public function __construct(ProductCategoryRepository $product_category)
    {
        $this->product_category = $product_category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Danh mục sản phẩm');
        return view('admin.product_category.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JSON
     */
    public function datatable()
    {
        $data = $this->product_category->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                        'link_edit' => route('admin.product.category.edit', $data->id),
                        'link_delete' => route('admin.product.category.destroy', $data->id),
                        'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Tạo danh mục');

        $parents = $this->product_category->all();

        return view('admin.product_category.create_edit', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->product_category->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'danh mục sản phẩm']));

        return redirect()->route('admin.product.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Chỉnh sửa danh mục');

        $product_category = $this->product_category->find($id);

        return view('admin.product_category.create_edit', compact('product_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->product_category->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'danh mục sản phẩm']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product_category->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'danh mục sản phẩm']));

        return redirect()->back();
    }
}
