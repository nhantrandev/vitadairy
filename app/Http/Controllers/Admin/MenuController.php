<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#Repostories
use App\Repositories\MenuRepository;

class MenuController extends Controller
{

    protected $menu;

    public function __construct(MenuRepository $menu)
    {
        $this->menu = $menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Danh sách Menu');
        return view('admin.menu.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JSON
     */
    public function datatable()
    {
        $data = $this->menu->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                        'link_edit' => route('admin.menu.edit', $data->id),
                        'link_delete' => route('admin.menu.destroy', $data->id),
                        'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a menu resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Tạo menu');

        return view('admin.menu.create_edit');
    }

    /**
     * Store a menuly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->menu->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'menu']));

        return redirect()->route('admin.menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Chỉnh sửa menu');

        $menu = $this->menu->find($id);

        return view('admin.menu.create_edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->menu->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'menu']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->menu->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'menu']));

        return redirect()->back();
    }
}
