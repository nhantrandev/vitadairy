<?php

namespace App\Http\Controllers\Frontend;

use Breadcrumb;
use App\Helper\TranslateUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\EmailRepository;
use App\Repositories\PageRepository;
use App\Repositories\NewRepository;
use App\Repositories\NewCategoryRepository;

use App\Models\NewCategory;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\News;

class PageController extends Controller
{
    protected $page;
    protected $email;
    protected $new;
    protected $new_category;

    public function __construct(
        PageRepository $page,
        EmailRepository $email,
        NewRepository $new,
        NewCategoryRepository $new_category
    )
    {
        $this->page = $page;
        $this->email = $email;
        $this->new = $new;
        $this->new_category = $new_category;
    }

    public function index()
    {
        $page = $this->page->findBySlug('/');

        $blocks = [];

        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');
        }

        foreach ($page->translations as $translation) {
            TranslateUrl::addWithLink($translation->locale, "/{$translation->locale}");
        }

        $metadata = $page->meta;

        if (view()->exists(THEME_PATH_VIEW . ".{$page->theme}")) {
            $with = [];

            if ($page->theme == 'home') {
                $product_category     = ProductCategory::get();
                $product  = Product::active()->orderBy('created_at', 'desc')->get();
                $new_category = NewCategory::get();

                $with = [
                    'product_category' => $product_category,
                    'product' => $product,
                    'new_category' => $new_category
                ];
            }

            return view(THEME_PATH_VIEW . ".{$page->theme}", compact('page', 'blocks', 'metadata'))->with($with);
        }

        abort(404);
    }

    public function show($slug)
    {
        $page = $this->page->findBySlug($slug);

        $input = [];

        $blocks = []; 

        if ($page->parentBlocks->count()) 
        {
            $blocks = $page->parentBlocks->groupBy('code');
        }

        foreach ($page->translations as $translation) 
        {
            TranslateUrl::addWithLink($translation->locale, "/{$translation->locale}");
        }

        $metadata = $page->meta;

        Breadcrumb::add($page->title);

        if (view()->exists(THEME_PATH_VIEW . ".{$page->theme}")) 
        {
            $with = [];

            // switch ($page->theme) {
            //     case 'product':
            //         {
            //             $product_categories = Category::where('level', 0)->get();
            //             $product_brands = Brand::featured()->get();

            //             $with = [
            //                 'product_categories'    => $product_categories,
            //                 'product_brands'        => $product_brands
            //             ];
            //         }
            //         break;
                
            //     default:
            //         # code...
            //         break;
            // }

            return view(THEME_PATH_VIEW . ".{$page->theme}", compact(
                'page', 'metadata', 'blocks'
            ))->with($with);
        }

        abort(404);
    }

    public function emailRegister(Request $request)
    {
        $input = $request->only('email');

        if($this->email->create($input)) {
            return restSuccess();
        }

        return restFail();
    }

    public function newCategory($new_category_slug)
    {
        $category = $this->new_category->findBySlug($new_category_slug);

        if(!empty($category))
            return view('frontend.new.category', compact('category'));
        return abort(404);
    }

    public function newDetail($new_category_slug, $new_slug)
    {
        $new = $this->new->findBySlug($new_category_slug, $new_slug);
        $new_related = News::where('id', '<>', $new->id)->active()->orderBy('created_at', 'desc')->limit(4)->get();

        if(!empty($new))
            return view('frontend.new.detail', compact('new', 'new_related'));
        return abort(404);
    }
}
