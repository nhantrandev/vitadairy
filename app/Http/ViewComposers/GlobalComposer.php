<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class GlobalComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    // protected $page;
    // protected $template;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository $users
     * @return void
     */
    // public function __construct(
    //     PageRepository $page,
    //     TemplateRepository $template
    // )
    // {
    //     $this->page = $page;
    //     $this->template = $template;
    // }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $locale = \App::getLocale();

        // $templates = \Cache::rememberForever("{$locale}_composer_templates", function () {
        //     return $this->template->get();
        // });

        // $view->with('composer_templates', $templates);
    }
}
