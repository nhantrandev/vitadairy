<?php namespace App\Traits;

use App\Models\Media;

trait PhotoArrayPathTrait
{
    public function arrayPath($fullPath = true)
    {
        $array = [];

        $array["small"] = "{$this->path}/small/{$this->file_name}";
        $array["medium"] = "{$this->path}/medium/{$this->file_name}";
        $array["large"] = "{$this->path}/large/{$this->file_name}";
        $array["full"] = "{$this->path}/{$this->file_name}";

        if ($fullPath) {
            $array["small"] = asset("storage/" . $array["small"]);
            $array["medium"] = asset("storage/" . $array["medium"]);
            $array["large"] = asset("storage/" . $array["large"]);
            $array["full"] = asset("storage/" . $array["full"]);
        }

        return $array;
    }

    public function storePhotos(array $photos, $type = 'image')
    {
        foreach($photos as $photo)
        {
            if(is_string($photo)) {
            
                $info = pathinfo($photo);
                $size = filesize(ltrim($photo, "/"));
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, ltrim($photo, "/"));
                finfo_close($finfo);
                $name = $info['filename'];
                $filename =  $info['basename'];
                $ext = $info['extension'];
                $path = $info['dirname'];

            } else if(is_file($photo)) {

                $path = "upload/{$this->table}";
                $filename = $photo->getClientOriginalName();
                $name_arr = explode(".", $filename);
                $ext =  array_pop($name_arr);
                $name = "post_{$this->id}_".time();
                $filename = "{$name}.{$ext}";
                $size = filesize($photo);
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $photo);
                $photo->move($path, $filename);

            } else {
                continue;
            }

            if(strstr($mime, "video/")){
                $type = "video";
            }
            
            $media = Media::create([
                'name' => $name,
                'file_name' => $filename,
                'path' => $path,
                'type' => $type,
                'mime' => $mime,
                'ext' => $ext,
                'size' => $size
            ]);

            $media->media_object()->create([
                'object_type' => $this->table,
                'object_id' => $this->id 
            ]);
            
        }
    }
}
