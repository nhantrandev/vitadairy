<?php

namespace App\Models;

use App\Traits\TranslatableExtendTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Menu extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TranslatableExtendTrait, TransformableTrait;

    protected $table = 'menu';

    protected $fillable = [
        'position'
    ];

    public $translatedAttributes = [
        'name',
        'url'
    ];

    public $translationForeignKey = 'menu_id';
}
