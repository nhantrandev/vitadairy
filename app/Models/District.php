<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = "district";

    protected $fillable = [
        "id",
        "city_id"
    ];

    public $translatedAttributes = [
        'name',
        'type'
    ];

    public $timestamps = false;
}
