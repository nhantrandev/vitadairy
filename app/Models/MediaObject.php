<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaObject extends Model
{
    protected $table = 'media_objects';

    protected $fillable = [
        'object_type',
        'object_id',
        'media_id',
        'is_cover',
        'position'
    ];

    public $timestamps = false;
}
