<?php

namespace App\Models;

use App\Traits\MetadataTrait;
use App\Traits\SlugTranslationTrait;
use App\Traits\TranslatableExtendTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NewCategory extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TranslatableExtendTrait, TransformableTrait, MetadataTrait, SlugTranslationTrait;

    protected $table = 'new_category';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at'
    ];

    public $translatedAttributes = [
        'name',
        'slug'
    ];

    public $slug_from_source = 'name';

    public function news()
    {
        return $this->hasMany(News::class, 'new_category_id');
    }
}
