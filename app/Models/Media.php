<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';


    protected $fillable = [
        'name',
        'file_name',
        'path',
        'type',
        'mime',
        'ext',
        'size'
    ];

    protected $appends = ['image_path'];

    public function media_object()
    {
        return $this->hasOne(MediaObject::class, 'media_id');
    }

    public function getImagePathAttribute()
    {
        return config('app.url')."/{$this->path}/{$this->file_name}";
    }
}
