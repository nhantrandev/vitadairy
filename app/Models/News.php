<?php

namespace App\Models;

use App\Traits\MetadataTrait;
use App\Traits\SlugTranslationTrait;
use App\Traits\TranslatableExtendTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class News extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TranslatableExtendTrait, TransformableTrait, MetadataTrait, SlugTranslationTrait;

    protected $table = 'new';

    protected $fillable = [
        'new_category_id',
        'image',
        'active'
    ];

    public $translatedAttributes = [
        'title',
        'slug',
        'description',
        'content'
    ];

    public $translationForeignKey = 'new_id';

    public $slug_from_source = 'title';

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function getLabelActiveAttribute()
    {
        return $this->active ? 'Active' : 'In-Active';
    }

    public function category()
    {
        return $this->belongsTo(NewCategory::class, 'new_category_id');
    }
}
