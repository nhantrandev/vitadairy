<?php

namespace App\Models;

use App\Traits\MetadataTrait;
use App\Traits\TranslatableExtendTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductCategory extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TranslatableExtendTrait, TransformableTrait, MetadataTrait;

    protected $table = 'product_category';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at'
    ];

    public $translatedAttributes = [
        'name'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'product_category_id');
    }
}
