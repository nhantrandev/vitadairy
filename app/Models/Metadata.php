<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Metadata extends Model
{
    use Translatable;

    protected $table = "metadata";

    protected $fillable = [
        'meta_key', 
        'object_id'
    ];

    public $translatedAttributes = [
        'title', 
        'description', 
        'key_word',
        'image_seo'
    ];
}
