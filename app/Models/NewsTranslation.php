<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    protected $table = 'new_translation';

    

    protected $fillable = [
        'title',
        'slug',
        'description',
        'content'
    ];

    public $timestamps = false;
}