<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryTranslation extends Model
{
    protected $table = 'product_category_translation';

    protected $fillable = [
        'name'
    ];

    public $timestamps = false;
}