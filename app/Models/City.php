<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = "city";

    protected $fillable = [
        "country_id",
        "position"
    ];

    public $translatedAttributes = [
        'name'
    ];

    public $timestamps = false;
}
