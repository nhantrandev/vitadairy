<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewCategoryTranslation extends Model
{
    protected $table = 'new_category_translation';

    protected $fillable = [
        'name',
        'slug'
    ];

    public $timestamps = false;
}