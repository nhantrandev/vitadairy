<?php

namespace App\Models;

use App\Traits\MetadataTrait;
use App\Traits\TranslatableExtendTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TranslatableExtendTrait, TransformableTrait, MetadataTrait;

    protected $table = 'product';

    protected $fillable = [
        'product_category_id',
        'image',
        'active',
        'is_popup'
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'url'
    ];

    public $translationForeignKey = 'product_id';

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeIsPopup($query)
    {
        return $query->where('is_popup', 1);
    }

    public function getLabelActiveAttribute()
    {
        return $this->active ? 'Active' : 'In-Active';
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }
}
