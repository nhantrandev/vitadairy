<?php namespace App\Helper;

class Breadcrumb
{
    public static $breadcrumb = [];

    public static $page_title = null;

    public static $page_description = null;

    public static function title($name, $description = null)
    {
        self::$page_title = $name;
        self::$page_description = $description;
    }

    public static function add($name, $link = null, $home = true)
    {
        if ($home && !count(self::$breadcrumb)) {
            $locale = \App::getLocale();
            self::add(trans('breadcrumb.home'), "/{$locale}", false);
        }
        self::$breadcrumb[] = [
            'name' => $name,
            'link' => $link
        ];
    }

    public static function out($view = 'frontend.layouts.breadcrumb')
    {
        $breadcrumbs = self::$breadcrumb;
        if(isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs) > 1) {
            return view($view, compact('breadcrumbs'))->render();
        }
    }
}
