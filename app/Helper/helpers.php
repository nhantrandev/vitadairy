<?php

function restSuccess($message = "Success", $result = [], $code = 200)
{
    $arr = [
        "status" => true,
        "status_code" => $code,
        "message" => $message,
    ];
    if (!empty($result)) {
        $arr["result"] = $result;
    }
    return response()->json($arr, $code);
}

function restFail($message = "Error", $code = 422, $errors = [])
{
    $arr = [
        "status" => false,
        "status_code" => $code,
        "message" => $message
    ];
    if (!empty($errors)) {
        $arr["errors"] = $errors;
    }
    return response()->json($arr, $code);
}

function apiSuccess($result = [], $message = "Success", $code = 200)
{
    $arr = [
        "status" => true,
        "status_code" => $code,
        "message" => $message,
    ];

    if (!empty($result)) {
        $arr["result"] = $result;
    }
    return response()->json($arr, $code);
}

function apiFail($errors = [], $message = "Error", $code = ERROR_SERVER_ERROR)
{
    $arr = [
        "status" => false,
        "status_code" => $code,
        "message" => $message
    ];
    if (!empty($errors)) {
        $arr["errors"] = $errors;
    }
    return response()->json($arr, $code);
}

function randStrGen($len = 7)
{
    $result = "";
    $chars = "1QAZXSW23EDCVFR45TGBNHY67UJMKI89OLP0";
    $charArray = str_split($chars);
    for ($i = 0; $i < $len; $i++) {
        $randItem = array_rand($charArray);
        $result .= "" . $charArray[$randItem];
    }
    return $result;
}

function assetStorage($path, $type = "full", $multi = false, $key = "medium")
{
    if (empty($path)) {
        return null;
    }
    if ($type === "storage" || $type === "full") {
        $path = "/storage/{$path}";
    }
    if ($type === "full") {
        $path = asset($path);
    }
    if ($multi) {
        $arr = explode("/", $path);

        $file_name = $arr[count($arr) - 1];
        unset($arr[count($arr) - 1]);

        $new_path = implode("/", $arr);

        $arr_path = [
            "small" => $new_path . "/small/" . $file_name,
            "medium" => $new_path . "/medium/" . $file_name,
            "large" => $new_path . "/large/" . $file_name,
            "full" => $new_path . "/" . $file_name,
        ];

        if ($key && !empty($arr_path[$key])) {
            return $arr_path[$key];
        }
        return $arr_path;
    }

    return $path;
}

function cvDbTime($date, $from = DB_DATE, $to = PHP_DATE)
{
    return $date ? \Carbon\Carbon::createFromFormat($from, $date)->format($to) : null;
}

function resourceAdmin($prefix, $controller, $name, $permission = null, array $except = ['show'])
{
    if ($permission === null) {
        $permission = $name;
    }
    Route::group(['prefix' => $prefix], function () use ($controller, $name, $permission, $except) {
        if (!in_array('index', $except)) {
            Route::get('/', "{$controller}@index")->name("admin.{$name}.index")->middleware("permission:admin.{$permission}.index");
        }

        if (!in_array('datatable', $except)) {
            Route::get('datatable', "{$controller}@datatable")->name("admin.{$name}.datatable")->middleware("permission:admin.{$permission}.index");
        }

        if (!in_array('create', $except)) {
            Route::get('create', "{$controller}@create")->name("admin.{$name}.create")->middleware("permission:admin.{$permission}.create");
            Route::post('/', "{$controller}@store")->name("admin.{$name}.store")->middleware("permission:admin.{$permission}.create");
        }

        if (!in_array('show', $except)) {
            Route::get('{id}', "{$controller}@show")->name("admin.{$name}.show")->middleware("permission:admin.{$permission}.show");
        }

        if (!in_array('edit', $except)) {
            Route::get('{id}/edit', "{$controller}@edit")->name("admin.{$name}.edit")->middleware("permission:admin.{$permission}.edit");
            Route::put('{id}', "{$controller}@update")->name("admin.{$name}.update")->middleware("permission:admin.{$permission}.edit");
        }

        if (!in_array('destroy', $except)) {
            Route::delete('{id}', "{$controller}@destroy")->name("admin.{$name}.destroy")->middleware("permission:admin.{$permission}.destroy");
        }
    });
}

function currentPageMenu($url, $class = "active")
{
    if (!is_array($url)) {
        $check = request()->is($url);
        return $check ? $class : "";
    } else {
        foreach ($url as $key => $value) {
            if (request()->is($value)) {
                return $class;
            }
        }
    }
    return "";
}

function cutString($str, $length = 15, $end = '...')
{
    $minword = 3;
    $sub = '';
    $len = 0;
    foreach (explode(' ', $str) as $word) {
        $part = (($sub != '') ? ' ' : '') . $word;
        $sub .= $part;
        $len += strlen($part);
        if (strlen($word) > $minword && strlen($sub) >= $length) {
            break;
        }
    }
    return $sub . (($len < strlen($str)) ? $end : '');
}

function removeAllConfig()
{
    \Artisan::call('view:clear');
    \Artisan::call('cache:clear');
    \Artisan::call('config:clear');
}

function fileNameFromPath($path, $name = true)
{
    if (!$path) {
        return null;
    }
    $string = str_replace('.blade.php', '', $path);

    if (!$name) {
        return $string;
    }

    $string = str_replace('-', ' ', $string);

    return ucwords($string);
}

function getThumbnail($img_path, $width, $height, $type = "fit")
{
    return app('App\Http\Controllers\ImageController')->getImageThumbnail($img_path, $width, $height, $type);
}


function jsonContent($string)
{
    if(is_numeric($string)){
        return $string;
    }
    $data = @json_decode($string);
    return (json_last_error() === JSON_ERROR_NONE) ? ((array)$data) : $string;
}

function checkSelectedOption($value, $selected_value)
{
    return $value == $selected_value ? 'selected' : '';
}

function CutstringLimit($str, $length = 10)
{
    return strlen($str) > $length ? substr($str,0,$length)."..." : $str;
}

function getTypesOfService(){
    return config('constants.services.types');
}

/*
 * Hadesker
 */
function summary($source_string, $max_len = 30)
{
    $source_string = strip_tags($source_string);
    if (strlen($source_string) < $max_len) return $source_string;
    $html = substr($source_string, 0, $max_len);
    $html = substr($html, 0, strrpos($html, ' '));
    return $html . '...';
}

function Date2String($inputDate, $formatOut = 'd . m . Y')
{
    return \Carbon\Carbon::parse($inputDate)->format($formatOut);
}

function getPageUrlByCode($code)
{
    $locale = \App::getLocale();
    $lang = ($locale == 'vi') ? '' : $locale;
    $page = \App\Models\Page::where('code', $code)->first();
    if ($page)
        return "$lang/$page->slug";
    return null;
}

function generateRandomString($length = 7) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// Check image in storage or upload folder
function getLinkStorageOrUpload($url)
{
    if(\Storage::disk('public')->exists($url)) {
        return \Storage::disk('local')->url($url);
    }
    return $url;
}

// Format price by locale
function number_format_locale($number, $decimal = null, $decimalpoint = '.', $thousand_separator = ','){
    $locale = app()->getLocale();
    if($locale == 'vi'){
        $decimalpoint = ',';
        $thousand_separator = '.';
    }
    return number_format($number, $decimal, $decimalpoint, $thousand_separator);
}

function getPriceAfterDiscount($price, $discount)
{
    return $price - $price*$discount/100;
}

function swapCollection($collection, $position) {
    $first = $collection->slice($position, $collection->count() - $position);
    $second = $collection->slice(0, $position);

    return $first->merge($second);
}