<?php

namespace App\Repositories;

use App\Models\Email;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmailRepository;

/**
 * Class EmailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EmailRepositoryEloquent extends BaseRepository implements EmailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Email::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->orderBy('created_at', 'desc');
    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);
        
        $model->delete();
    }
}
