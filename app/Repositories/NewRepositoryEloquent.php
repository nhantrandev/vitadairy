<?php

namespace App\Repositories;

use App\Models\News;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NewRepositoryEloquent extends BaseRepository implements NewRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return News::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->withTranslation();
    }

    public function create(array $input)
    {
        $input['active'] = !empty($input['active']) ? 1 : 0;

        $model = $this->model->create($input);

        if (!empty($input['metadata'])) {
            $model->metaCreateOrUpdate($input['metadata']);
        }

        $model->updateSlugTranslation();

        return $model;
    }

    public function update(array $input, $id)
    {
        $input['active'] = !empty($input['active']) ? 1 : 0;

        $model = $this->model->findOrFail($id);

        $model->update($input);

        if (!empty($input['metadata'])) {
            $model->metaCreateOrUpdate($input['metadata']);
        }

        $model->updateSlugTranslation();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);

        //delete metadata
        $model->meta()->delete();

        //delete
        $model->delete();
    }

    public function findBySlug($new_category_slug, $new_slug)
    {
        $model = $this->model->whereTranslation('slug', $new_slug)
                ->whereHas('category', function($query) use ($new_category_slug){
                    $query->whereTranslation('slug', $new_category_slug);
                })->active()->first();
        
        return $model;
    }
}
