<?php

namespace App\Repositories;

use App\Models\NewCategory;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewCategoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NewCategoryRepositoryEloquent extends BaseRepository implements NewCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewCategory::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->withTranslation()->orderBy('created_at', 'desc');
    }

    public function create(array $input)
    {
        $model = $this->model->create($input);

        $model->updateSlugTranslation();

        return $model;
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);

        $model->update($input);

        $model->updateSlugTranslation();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);

        $model->delete();
    }

    public function findBySlug($new_category_slug)
    {
        $model = $this->model->whereTranslation('slug', $new_category_slug)->first();

        return $model;
    }
}
