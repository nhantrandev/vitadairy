<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewCategoryRepository
 * @package namespace App\Repositories;
 */
interface NewCategoryRepository extends RepositoryInterface
{
    public function datatable();

    public function findBySlug($new_category_slug);
}