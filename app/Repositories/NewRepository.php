<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewRepository
 * @package namespace App\Repositories;
 */
interface NewRepository extends RepositoryInterface
{
    public function datatable();

    public function findBySlug($new_category_slug, $new_slug);
}