<?php

namespace App\Observers;

use App\Models\Page;

class PageObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User $user
     * @return void
     */
    public function created(Page $page)
    {
        // 
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\User $user
     * @return void
     */
    public function updated(Page $page)
    {
        // 
    }

}