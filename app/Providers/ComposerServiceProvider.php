<?php

namespace App\Providers;

use App\Http\ViewComposers\GlobalComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Page;
use App\Models\City;
use App\Models\Country;
use App\Models\System;
use App\Models\Brand;
use App\Models\Category;
use Cart;
use Auth;
use App;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Get locate
        View::composer('*', function ($view) {
            $locales = \LaravelLocalization::getSupportedLocales();
            $view->with('composer_locales', $locales);
            $view->with('composer_locale', App::getLocale());
        });

        // Admin permission
        View::composer(['admin.layouts.partials.menu','admin.dashboard.index'], function ($view) {
            $auth = Auth::user();
            $value = [];
            if($auth){
                $value =  $auth->getPermissions()->pluck('slug')->toArray();
            }
            $view->with('composer_auth_permissions', $value);
        });

        // Admin permission
        View::composer(['frontend.layouts.header'], function ($view) {
            $menu = \App\Models\Menu::orderBy('position', 'asc')->get();
            
            $view->with('composer_menu', $menu);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
