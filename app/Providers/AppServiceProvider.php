<?php

namespace App\Providers;

use Log;
use App\Observers\PageObserver;
use App\Observers\ProductObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Log by user
        $user = exec('whoami');
        $user = str_slug($user);
        Log::getLogger()->popHandler();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
