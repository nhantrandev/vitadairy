$(document).ready(function() {
    $(document).on('click', '.add-to-cart', function() {
        var product_id      = $(this).attr('data-product-id');
        var product_price   = $("input[name='price']:checked").attr('data-id');

        // var product_price_other = 0;
        var product_price_other = $("input[name='price-other']:checked").map(function (_, el) {
            return $(el).attr('data-id');
        }).get();
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_ADD_CART,
            type: 'POST',
            data: {
                product_id: product_id,
                product_price: product_price,
                product_price_other: product_price_other
            },
            success: function(data) {
                console.log(data);

                window.location.href = URL_GET_CHECKOUT;
            },
            error: function(error){
                console.log(error);
            }
        });
    });
});