function ui(){// Select UI
$.fn.select2.defaults.set("width","100%");$(".select-ui").each(function(){var el=$(this);var selectUI=el.select2({placeholder:el.data("placeholder")});// Update UI Scroll - Open dropdown
selectUI.on("select2:open",function(e){var id=$(".select2-results  > .select2-results__options").attr("id");$(".select2-results").attr({"id":id+"-group"}).queue(function(next){new SimpleBar($("#"+id+"-group")[0]);next()})})});// Range UI
$(".range-ui").each(function(key){var el=$(this);el.attr({"id":"range-ui-"+key}).queue(function(next){$("#range-ui-"+key).ionRangeSlider();next()})});// Scroll
$(".scroll-ui").each(function(key){var el=$(this);el.attr({"id":"scroll-ui-"+key}).queue(function(next){new SimpleBar($("#"+el.attr("id"))[0]);next()})});// File Browse UI
$(".file-ui .file-ui-input").change(function(e){if(typeof e.target.files[0]!=="undefined"){var fileName=e.target.files[0].name;$(this).siblings(".file-ui-label").text(fileName)}})}// Image svg
function imgSVG(){$("img.svg").each(function(){var $img=$(this);var imgID=$img.attr("id");var imgClass=$img.attr("class");var imgURL=$img.attr("src");$.get(imgURL,function(data){// Get the SVG tag, ignore the rest
var $svg=$(data).find("svg");// Add replaced image's ID to the new SVG
if(typeof imgID!=="undefined"){$svg=$svg.attr("id",imgID)}// Add replaced image's classes to the new SVG
if(typeof imgClass!=="undefined"){$svg=$svg.attr("class",imgClass+" replaced-svg")}// Remove any invalid XML tags as per http://validator.w3.org
$svg=$svg.removeAttr("xmlns:a");// Replace image with new SVG
$img.replaceWith($svg)},"xml")})}function waypointEl(){var way=$("[data-waypoint]");way.each(function(){var _el=$(this),_ofset=_el.data("waypoint"),_up=_el.data("waypointup");_el.waypoint(function(direction){if(direction=="down"){_el.addClass("active")}else{if(_up){_el.removeClass("active")}}},{offset:_ofset})})}function animateEffect(){var way=$("[data-animated-effect]");way.addClass("animated").waypoint({offset:"70%",triggerOnce:true,handler:function(){var el=$(this.element).length?$(this.element):$(this);el.each(function(i,elem){var elem=$(elem),type=$(this).data("animated-effect"),delay=$(this).data("animated-delay");setTimeout(function(){elem.addClass(type)},delay)});if(typeof this.destroy!=="undefined"&&$.isFunction(this.destroy)){this.destroy()}}})}// Header
function header(){}// var stick = $('.navbar__bottom');
// var sticky = new Waypoint.Sticky({
//     element: stick[0],
//     wrapper: '<div />',
//     stuckClass: 'sticky',
//     handler: function(direction) {
//         if(direction == 'down') {
//             stick.removeClass('unsticky');
//         } else {
//             stick.addClass('unsticky');
//         }
//     }
// });
//Menu
$(function($){$toggleMenu=$(".navbar-toggle");$toggleHeader=$(".header");$toggleMenu.bind("click",function(e){var el=$(this);el.toggleClass("active");$toggleHeader.toggleClass("sticky");$(".menuMain").toggleClass("active");$("body").toggleClass("moveLeft");e.preventDefault()});$expand=$(".expand");$expand.click(function(){el=$(this);elUl=$(this).next("ul");$childUl=el.parent("li").parent("ul").find("ul.menuChild");$childExpand=el.parent("li").parent("ul").find(".expand");//$childUl.hide();
if(el.hasClass("active")){el.removeClass("active");elUl.stop().slideUp(200)}else{$childExpand.removeClass("active");$childUl.stop().slideUp(200);el.addClass("active");elUl.stop().slideDown(200)}})});//Searchbox
function searchBox(){var sButton=$(".navbar-search-btn"),sForm=$(".navbar-search");sButton.bind("click",function(e){e.preventDefault();e.stopPropagation();if(sForm.hasClass("active")){sForm.removeClass("active");sButton.removeClass("active")}else{sForm.addClass("active");sButton.addClass("active")}});$(document).click(function(){if(sForm.hasClass("active")){sForm.removeClass("active");sButton.removeClass("active")}});sForm.bind("click",function(e){e.stopPropagation()})}function sortNews(){var filterFns={// show if number is greater than 50
numberGreaterThan50:function(){var number=$(this).find(".number").text();return parseInt(number,10)>50},// show if name ends with -ium
ium:function(){var name=$(this).find(".name").text();return name.match(/ium$/)}};$(".sProductGird").isotope({itemSelector:".filter "// masonry: {
//   columnWidth: 200
// }
});$(".navTab a").click(function(e){e.preventDefault();var filterValue=$(this).attr("data-filter");filterValue=filterFns[filterValue]||filterValue;$(".sProductGird").isotope({filter:filterValue})})}$(".sBanner__item__title h1").lettering("words");function techSlider(){$(".proSlide  ").on("init",function(event,slick){}).slick({dots:false,prevArrow:"<div class=\"arrow arrow--prev\"><i class=\"arrow_carrot-left\"></i></div>",nextArrow:"<div class=\"arrow arrow--next\"><i class=\"arrow_carrot-right\"></i></div>",infinite:true,speed:300,slidesToShow:1,adaptiveHeight:true,fade:true,cssEase:"linear"}).on("afterChange",function(event,slick,currentSlide){slideIndex=currentSlide;$(".proSlideArrow").slick("slickGoTo",slideIndex)});$(".proSlideArrow").slick({dots:false,arrows:false,infinite:true,speed:300,slidesToShow:1,adaptiveHeight:true,fade:true,swipe:false,cssEase:"linear"})}function certSlider(){$(".sCertificateMain").on("init",function(event,slick){}).slick({dots:false,prevArrow:"<div class=\"arrow arrow--prev\"><i class=\"arrow_carrot-left\"></i></div>",nextArrow:"<div class=\"arrow arrow--next\"><i class=\"arrow_carrot-right\"></i></div>",infinite:true,speed:300,slidesToShow:1,slidesToScroll:1,fade:true,//adaptiveHeight: true,
cssEase:"linear",appendArrows:$(".sCertificate__arrow"),asNavFor:".sCertificateArrow"}).on("afterChange",function(event,slick,currentSlide){slideIndex=currentSlide+1;$(".sCertificateInfo__item").removeClass("active");$(".sCertificateInfo__item:nth-child("+slideIndex+")").addClass("active")});$(".sCertificateArrow").slick({infinite:true,slidesToShow:1,slidesToScroll:1,dots:false,asNavFor:".sCertificateMain",swipe:false,fade:true,arrows:false})}function tvc(){$(".sTvcMain").on("init",function(event,slick){}).slick({dots:false,prevArrow:"<div class=\"arrow arrow--prev\"><i class=\"arrow_carrot-left\"></i></div>",nextArrow:"<div class=\"arrow arrow--next\"><i class=\"arrow_carrot-right\"></i></div>",infinite:true,speed:300,slidesToShow:1,slidesToScroll:1,fade:true,//adaptiveHeight: true,
cssEase:"linear",appendArrows:$(".sTvc__arrow"),asNavFor:".sTvcArrow"});$(".sTvcArrow").slick({infinite:true,slidesToShow:1,slidesToScroll:1,dots:false,asNavFor:".sTvcMain",swipe:false,fade:true,arrows:false})}function newsSlider(){var $carousel=$(".sNewsSlide");$carousel.slick({slidesToShow:3,slidesToScroll:1,prevArrow:"<div class=\"arrow arrow--prev\"><i class=\"arrow_carrot-left\"></i></div>",nextArrow:"<div class=\"arrow arrow--next\"><i class=\"arrow_carrot-right\"></i></div>",autoplay:false,centerMode:false,mobileFirst:false,variableWidth:false,adaptiveHeight:false,focusOnSelect:true,infinite:false,responsive:[{breakpoint:1024,settings:{slidesToShow:2,slidesToScroll:2}},{breakpoint:480,settings:{slidesToShow:1,slidesToScroll:1}}]})}function gotoTop(){var topTop=$(".onTop");topTop.click(function(){$("body,html").animate({scrollTop:0},500);return false});$(window).scroll(function(){if($(this).scrollTop()>10){topTop.addClass("active")}else{topTop.removeClass("active")}})}function showMenuChild(){var el=$(".btn-nav");el.each(function(){$(this).click(function(event){event.preventDefault();$(this).next().slideToggle();$(this).toggleClass("open-nav")})})}function init(){waypointEl();// base
ui();// Image SVG
imgSVG();// Header
header();//search box
searchBox();showMenuChild();// Banner
//banner();
//teach slider
techSlider();//Cert
certSlider();//tvc
tvc();//news
newsSlider();// Go to top
gotoTop();sortNews();$(window).on("debouncedresize",function(event){})}$("body").imagesLoaded(function(){init();$("body").addClass("loaded");$(".pageLoad").fadeOut()});
