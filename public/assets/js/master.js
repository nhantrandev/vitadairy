jQuery(function ($) {

    $('#loginFormModal').validate({
        ignore: "",

        rules: {
            "email": {
                required: true,
                email: true
            },
            "password": {
                required: true
            },
        },
        highlight: function (element) 
        {
            $('.error').css('color', 'red');
        },
        unhighlight: function (element) 
        {
            
        },
        errorPlacement: function (error, element) 
        {
            error.insertAfter($(element));
        },
    });

    $('#loginForm').validate({
        ignore: "",

        rules: {
            "email": {
                required: true,
                email: true
            },
            "password": {
                required: true
            },
        },
        highlight: function (element) 
        {
            $('.error').css('color', 'red');
        },
        unhighlight: function (element) 
        {
            
        },
        errorPlacement: function (error, element) 
        {
            error.insertAfter($(element));
        },
    });

    $('#signUpFormModal').validate({
        ignore: "",

        rules: {
            "email": {
                required: true,
                email: true
            }
        },
        highlight: function (element) 
        {
            $('.error').css('color', 'red');
        },
        unhighlight: function (element) 
        {
            
        },
        errorPlacement: function (error, element) 
        {
            error.insertAfter($(element).parents('.form-row'));
        },
    });

    $('#registerform').validate({
        ignore: "",

        rules: {
            "email": {
                required: true,
                email: true
            }
        },
        highlight: function (element) 
        {
            $('.error').css('color', 'red');
        },
        unhighlight: function (element) 
        {
            
        },
        errorPlacement: function (error, element) 
        {
            error.insertAfter($(element).parents('.form-row'));
        },
    });

    $('#forgotForm').validate({
        ignore: "",

        rules: {
            "email": {
                required: true,
                email: true
            }
        },
        highlight: function (element) 
        {
            $('.error').css('color', 'red');
        },
        unhighlight: function (element) 
        {
            
        },
        errorPlacement: function (error, element) 
        {
            error.insertAfter($(element));
        },
    });
});


