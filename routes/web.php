<?php
Route::get('remove-cache', function () {
    removeAllConfig();
    echo 'Removed all caching';
});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [
            'localizationRedirect'
        ]
    ],
    function () {
        // Page Home
        Route::get('/', 'PageController@index')->name('page.home');

        Route::get('trang-chu', function(){
            return view('frontend.index');
        });

        Route::get('kien-thuc-cho-me', function(){
            return view('frontend.kien_thuc_cho_me');
        });

        Route::get('chi-tiet', function(){
            return view('frontend.chi_tiet');
        });

        // Send Email
        Route::post('email-register', 'PageController@emailRegister')->name('email.register.post');

        // New category
        Route::get('/{new_category_slug}', 'PageController@newCategory')->name('new.category.get');

        Route::get('/{new_category_slug}/{new_slug}', 'PageController@newDetail')->name('new.detail.get');
        
        // Page Show
        Route::get('/{slug}', 'PageController@show')->name('frontend.show');
});