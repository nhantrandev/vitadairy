<?php
Route::group(
    [
        'prefix' => '',
        'middleware' => []
    ],
    function () {
        Route::group(["prefix" => "admin"], function () {

            Auth::routes();

            Route::group(['middleware' => ['auth', "permission:admin.index"]], function () {

                Route::get('/', 'DashboardController@index')->name("admin.dashboard.index")->middleware("permission:admin.index");

                resourceAdmin('users', 'UserController', 'user');

                resourceAdmin('roles', 'RoleController', 'role', 'role', ['show', 'create', 'destroy']);

                resourceAdmin('system', 'SystemController', 'system', 'system', ['show', 'index', 'create', 'destroy']);


                /* ---------------------------------------------OTHERS------------------------------------------------- */

                // Page
                resourceAdmin('themes', 'ThemeController', 'theme');
                Route::get('pages/create/load-block', 'PageController@loadBlock')->name("admin.page.load.block")->middleware("permission:admin.page.create", 'permission:admin.page.edit');
                resourceAdmin('pages', 'PageController', 'page');

                // News
                resourceAdmin('news', 'NewController', 'new');

                // New category
                resourceAdmin('new-category', 'NewCategoryController', 'new.category');

                // Product
                resourceAdmin('products', 'ProductController', 'product');

                // Product category
                resourceAdmin('product-category', 'ProductCategoryController', 'product.category');

                // Email
                resourceAdmin('email', 'EmailController', 'email', 'email', ['create', 'update', 'edit', 'store', 'show']);
                
                // Menu
                resourceAdmin('menu', 'MenuController', 'menu');
            });
        });
    });
