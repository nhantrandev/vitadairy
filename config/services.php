<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'map_key' => 'AIzaSyA1zH5PlR4b4KkAHOCO3kVH6JnthQH7YmI'
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '2228903527347350'),
        'client_secret' => env('FACEBOOK_APP_SECRET', '036b272bb9c1dca9a4c494452b0bd84a'),
        'redirect' => env('FACEBOOK_APP_CALLBACK_URL', 'http://healthy.happy/facebook/callback'),
    ],

    'google' => [
        'client_id'     => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT')
    ],

    'subscribe' => [
        'mailchimp_api_key' => env('MAILCHIMP_API_KEY'),
        'mailchimp_list_id' => env('MAILCHIMP_LIST_ID'),
    ]
];