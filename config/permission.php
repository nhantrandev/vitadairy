<?php
return [
    // 
    'permission' => [
        'model' => 'Admin',
        'permissions' => [
            'admin.index' => 'Truy cập'
        ]
    ],

    'user' => [
        'model' => 'Tài khoản',
        'permissions' => [
            'admin.user.index' => 'Truy cập',
            'admin.user.create' => 'Tạo',
            'admin.user.edit' => 'Sửa',
            'admin.user.destroy' => 'Xóa',
            'admin.user.set.permission' => 'Cấp quyền'
        ]
    ],

    'role' => [
        'model' => 'Vai trò',
        'permissions' => [
            'admin.role.index' => 'Truy cập',
            'admin.role.edit' => 'Sửa'
        ]
    ],

    'system' => [
        'model' => 'Hệ thống',
        'permissions' => [
            'admin.system.edit' => 'Sửa',
        ]
    ],
    

    //  OTHERS

    'page' => [
        'model' => 'Trang',
        'permissions' => [
            'admin.page.index' => 'Truy cập',
            'admin.page.create' => 'Tạo',
            'admin.page.edit' => 'Sửa',
            'admin.page.destroy' => 'Xóa'
        ]
    ],

    'theme' => [
        'model' => 'Bản mẫu',
        'permissions' => [
            'admin.theme.index' => 'Truy cập',
            'admin.theme.create' => 'Tạo',
            'admin.theme.edit' => 'Sửa',
            'admin.theme.destroy' => 'Xóa',
        ]
    ],

    'email' => [
        'model' => 'Quản lý Email',
        'permissions' => [
            'admin.email.index' => 'Truy cập',
            'admin.email.create' => 'Tạo',
            'admin.email.edit' => 'Sửa',
            'admin.email.destroy' => 'Xóa',
        ]
    ],

    'new' => [
        'model' => 'Tin tức',
        'permissions' => [
            'admin.new.index' => 'Truy cập',
            'admin.new.create' => 'Tạo',
            'admin.new.edit' => 'Sửa',
            'admin.new.destroy' => 'Xóa',
        ]
    ],

    'new_category' => [
        'model' => 'Danh mục tin tức',
        'permissions' => [
            'admin.new.category.index' => 'Truy cập',
            'admin.new.category.create' => 'Tạo',
            'admin.new.category.edit' => 'Sửa',
            'admin.new.category.destroy' => 'Xóa',
        ]
    ],

    'product' => [
        'model' => 'Sản phẩm',
        'permissions' => [
            'admin.product.index' => 'Truy cập',
            'admin.product.create' => 'Tạo',
            'admin.product.edit' => 'Sửa',
            'admin.product.destroy' => 'Xóa',
        ]
    ],

    'product_category' => [
        'model' => 'Danh mục sản phẩm',
        'permissions' => [
            'admin.product.category.index' => 'Truy cập',
            'admin.product.category.create' => 'Tạo',
            'admin.product.category.edit' => 'Sửa',
            'admin.product.category.destroy' => 'Xóa',
        ]
    ],

    'menu' => [
        'model' => 'Menu',
        'permissions' => [
            'admin.menu.index' => 'Truy cập',
            'admin.menu.create' => 'Tạo',
            'admin.menu.edit' => 'Sửa',
            'admin.menu.destroy' => 'Xóa',
        ]
    ],
];