<?php

return [

    "slider_image" => [
        "path" => "slider",
        "base_64" => true,
        "crop" => false,
        "max_width" => 1600,
        "sizes" => [
            "large" => [1600, null],
            "medium" => [500, null],
            "small" => [200, null]
        ]
    ]

];